package com.example.wcare;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.util.Arrays;

@SpringBootApplication
public class WcareApplication {

    public static void main(String[] args) {
        SpringApplication.run(WcareApplication.class, args);
    }
}


/*
Notes:
TODO password hash if not done
*/