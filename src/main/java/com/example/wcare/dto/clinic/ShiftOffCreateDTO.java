package com.example.wcare.dto.clinic;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record ShiftOffCreateDTO(
        String userId,
        LocalDateTime from,
        LocalDateTime to
) {
}
