package com.example.wcare.dto.clinic;

import com.example.wcare.data.models.FileModel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public record ClinicUpdateDTO(
        String name,
        String number,
        String address,
        Map<String, String> userColors,
        Map<String, Object> settings,
        String specialties,
        List<FileModel> files
) {
}
