package com.example.wcare.dto.clinic;
import com.example.wcare.data.entities.ShiftOff;
import com.example.wcare.dto.user.UserDataDTO;
import lombok.Builder;

import java.util.List;

@Builder
public record ClinicUserDTO(
        UserDataDTO user,
        UserShiftDTO shift,
        List<ShiftOffDTO> shiftOff
) {
}
