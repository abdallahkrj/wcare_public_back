package com.example.wcare.dto.clinic;

import com.example.wcare.data.object.DayShift;
import lombok.Builder;

import java.time.DayOfWeek;
import java.util.Map;

@Builder
public record UserShiftCreateDTO(
        String clinicId,
        String userId,
        Map<DayOfWeek, DayShift> daysShifts
) {
}
