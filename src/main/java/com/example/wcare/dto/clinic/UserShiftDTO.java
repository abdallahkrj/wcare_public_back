package com.example.wcare.dto.clinic;

import com.example.wcare.data.object.DayShift;
import lombok.Builder;

import java.time.DayOfWeek;
import java.util.Map;

@Builder
public record UserShiftDTO(
        String id,
        String userId,
        String clinicId,
        Map<DayOfWeek, DayShift> daysShifts
) {
}
