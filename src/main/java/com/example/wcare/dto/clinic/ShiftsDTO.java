package com.example.wcare.dto.clinic;


import lombok.Builder;

import java.util.List;


@Builder
public record ShiftsDTO(
        String clinicId,
        List<ShiftOffDTO> offList,
        List<UserShiftDTO> userShifts
) {
}
