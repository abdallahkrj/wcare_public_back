package com.example.wcare.dto.clinic;

import com.example.wcare.data.models.FileModel;
import com.example.wcare.data.models.SubscriptionPlanModel;
import com.example.wcare.data.models.UserInfoModel;

import java.util.List;
import java.util.Map;

public record ClinicDTO(
        String id,
        String name,
        String number,
        String address,
        Map<String, String> userColors,
        Map<String, Object> settings,
        String specialties,
        List<UserInfoModel> clients,
        List<FileModel> files,
        SubscriptionPlanModel plan
) {
}
