package com.example.wcare.dto.clinic;

import lombok.AllArgsConstructor;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record ShiftOffDTO(
        String id,
        String clinicId,
        String userId,
        LocalDateTime from,
        LocalDateTime to
) {
}
