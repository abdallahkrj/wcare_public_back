package com.example.wcare.dto.patient;

public record PatientCompleteRegistrationDTO(
        String patientId,
        String password,
        String code
) {
}
