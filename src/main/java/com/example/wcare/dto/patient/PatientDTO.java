package com.example.wcare.dto.patient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.Date;

public record PatientDTO(
        String id,
        String firstName,
        String lastName,
        LocalDate birthDate,
        String address,
        String number,
        String email,
//        @JsonIgnore
        String password,
        String token

        ) {
}
