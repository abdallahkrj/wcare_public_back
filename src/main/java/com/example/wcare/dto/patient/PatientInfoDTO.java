package com.example.wcare.dto.patient;

import jakarta.validation.constraints.NotNull;
import lombok.Builder;

import java.time.LocalDate;
import java.util.Date;

@Builder
public record PatientInfoDTO(
        @NotNull
        String id,
        @NotNull
        String firstName,
        @NotNull
        String lastName,
        @NotNull
        @NotNull
        LocalDate birthDate,
        @NotNull
        String address,
        @NotNull
        String number,
        @NotNull
        String email,
        @NotNull
        String clinicId,
        @NotNull
        String clinicName
        ) {
}
