package com.example.wcare.dto.patient;

import java.time.LocalDate;
import java.util.Date;

 public record PatientSignupDTO(
    String firstName,
    String lastName,
    LocalDate birthDate,
    String address,
    String number,
    String email
){}
