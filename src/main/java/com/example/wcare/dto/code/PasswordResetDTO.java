package com.example.wcare.dto.code;

public record PasswordResetDTO(
        String code,
        String password
) {
}
