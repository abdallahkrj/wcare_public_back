package com.example.wcare.dto.code;

public record CodeDTO(
        String code
) {
}
