package com.example.wcare.dto.procedures;

import com.example.wcare.data.models.ClinicInfoModel;
import com.example.wcare.data.object.ProcedureDetails;
import com.example.wcare.data.models.UserInfoModel;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record ProcedureDataDTO(
        String id,
        UserInfoModel user,
        ClinicInfoModel clinic,
        String patientId,
        String mainType,
        String subType,
        ProcedureDetails details,
        LocalDateTime procedureDate
) {
}
