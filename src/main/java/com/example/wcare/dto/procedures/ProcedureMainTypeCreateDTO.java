package com.example.wcare.dto.procedures;

import com.example.wcare.data.object.Specialities;

public record ProcedureMainTypeCreateDTO(
        Specialities speciality,
        String mainType
) {
}
