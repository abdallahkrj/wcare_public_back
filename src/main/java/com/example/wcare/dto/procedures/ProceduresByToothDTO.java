package com.example.wcare.dto.procedures;

import lombok.Builder;

import java.util.List;

@Builder
public record ProceduresByToothDTO (
    int toothNumber,
     List<ProcedureMetaDataDTO> procedureList
){}
