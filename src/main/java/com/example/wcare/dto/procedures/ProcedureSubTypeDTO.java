package com.example.wcare.dto.procedures;

import lombok.Builder;

import java.util.UUID;

@Builder
public record ProcedureSubTypeDTO(
        String userId,
        UUID mainTypeId,
        String mainType,
        UUID subTypeId,
        String subType
) {
}
