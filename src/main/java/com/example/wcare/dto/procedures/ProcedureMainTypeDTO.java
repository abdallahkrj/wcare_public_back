package com.example.wcare.dto.procedures;

import com.example.wcare.data.models.ProcedureSubTypeModel;
import lombok.Builder;

import java.util.List;
import java.util.UUID;

@Builder
public record ProcedureMainTypeDTO(
            UUID id,
            String type,
            List<ProcedureSubTypeModel> procedureSubTypeModels
) {
}
