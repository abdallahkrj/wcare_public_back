package com.example.wcare.dto.procedures;

import com.example.wcare.data.object.ProcedureDetails;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record ProcedureMetaDataDTO(
        String id,
        String userId,
        String clinicId,
        String patientId,
        String mainType,
        String subType,
        ProcedureDetails details,
        LocalDateTime procedureDate
) {
}
