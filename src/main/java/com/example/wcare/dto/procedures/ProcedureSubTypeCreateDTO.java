package com.example.wcare.dto.procedures;

import java.util.UUID;

public record ProcedureSubTypeCreateDTO(
        String userId,
        UUID mainTypeId,
        String subType
) {
}
