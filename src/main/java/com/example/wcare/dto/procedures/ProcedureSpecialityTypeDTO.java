package com.example.wcare.dto.procedures;

import com.example.wcare.data.object.Specialities;
import com.example.wcare.data.object.TypeObject;
import lombok.Builder;

import java.util.List;

@Builder
public record ProcedureSpecialityTypeDTO(
        Specialities speciality,
        List<TypeObject> mainTypes
) {
}
