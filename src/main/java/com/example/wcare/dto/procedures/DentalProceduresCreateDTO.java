package com.example.wcare.dto.procedures;

import com.example.wcare.data.object.DentalProcedure;

import java.time.LocalDateTime;
import java.util.UUID;

public record DentalProceduresCreateDTO(
        String userId,
        String patientId,
        UUID mainTypeId,
        UUID subTypeId,
        LocalDateTime procedureDate,
        DentalProcedure details
        ) {
}
