package com.example.wcare.dto.privileges;

public record PrivilegesDTO(
        String id,
        String name,
        String description
) {
}
