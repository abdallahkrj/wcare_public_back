package com.example.wcare.dto.privileges;

public record PrivilegesCreateDTO(
        String name,
        String description
) {
}
