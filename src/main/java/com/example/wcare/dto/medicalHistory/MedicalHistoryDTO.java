package com.example.wcare.dto.medicalHistory;

import com.example.wcare.data.object.MedicalHistoryType;
import com.example.wcare.data.models.FileModel;
import com.mongodb.lang.Nullable;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;

public record MedicalHistoryDTO(
        @NotNull
        String id,
        @NotNull
        MedicalHistoryType type,
        @NotNull
        String description,
        @NotNull
        LocalDateTime dateTime,
        @NotNull
        Boolean status,
        @Nullable
        FileModel attachment
) {
}
