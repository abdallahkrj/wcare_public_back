package com.example.wcare.dto.medicalHistory;

import jakarta.validation.constraints.NotNull;
import org.springframework.context.annotation.Lazy;

public record FileCreateDTO(
        @NotNull
        String type,
        @NotNull
        byte[] file
){
}
