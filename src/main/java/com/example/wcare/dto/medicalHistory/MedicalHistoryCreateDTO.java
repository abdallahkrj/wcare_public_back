package com.example.wcare.dto.medicalHistory;

import com.example.wcare.data.object.MedicalHistoryType;
import com.mongodb.lang.Nullable;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDateTime;

import static java.time.LocalTime.now;

public record MedicalHistoryCreateDTO(
        @NotNull
        MedicalHistoryType type,
        @NotNull
        String description,
        LocalDateTime date,
        @Nullable
        FileCreateDTO attachment
) {
}
