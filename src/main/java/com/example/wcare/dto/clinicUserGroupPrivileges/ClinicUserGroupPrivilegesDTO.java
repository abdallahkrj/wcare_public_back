package com.example.wcare.dto.clinicUserGroupPrivileges;

import com.example.wcare.data.models.ClinicInfoModel;
import com.example.wcare.data.models.ClinicModel;
import com.example.wcare.data.models.PrivilegesModel;
import com.example.wcare.data.models.UserInfoModel;
import jakarta.validation.constraints.NotNull;

import java.util.List;

public record ClinicUserGroupPrivilegesDTO(
        @NotNull
        String id,
        @NotNull
        String name,
        @NotNull
        ClinicInfoModel clinic,
        @NotNull
        List<UserInfoModel> users,
        @NotNull
        List<PrivilegesModel> privileges
) {
}