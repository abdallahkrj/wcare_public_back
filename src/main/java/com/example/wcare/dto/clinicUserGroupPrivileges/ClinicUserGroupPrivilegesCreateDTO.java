package com.example.wcare.dto.clinicUserGroupPrivileges;

import jakarta.validation.constraints.NotNull;

import java.util.List;

public record ClinicUserGroupPrivilegesCreateDTO(
        @NotNull
        String name,

        String clinicId,
        @NotNull
        List<String> privilegesIds
) {
}