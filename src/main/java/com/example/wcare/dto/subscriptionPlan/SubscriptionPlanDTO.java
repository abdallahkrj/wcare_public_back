package com.example.wcare.dto.subscriptionPlan;

import com.example.wcare.data.models.PrivilegesModel;
import java.util.List;

public record SubscriptionPlanDTO(
        String id,
        String name,
        double duration,
        int maxUserCount,
        List<PrivilegesModel> privileges
) {
}
