package com.example.wcare.dto.user;

import com.example.wcare.data.object.Specialities;
import jakarta.validation.constraints.NotNull;

public record UserSignupDTO(
        @NotNull
        String firstName,
        @NotNull
        String lastName,
        @NotNull
        String password,
        @NotNull
        String email,
        @NotNull
        String number,
        @NotNull
        Specialities speciality,
        @NotNull
        String clinicUserGroupPrivilegesId

// TODO: make clinic user group privileges create
//        @NotNull
//        List<String> privilegesIds
) {
}