package com.example.wcare.dto.user;

import com.example.wcare.data.models.ClinicModel;
import com.example.wcare.data.models.ClinicUserPrivilegesGroupModel;

import java.util.List;

public record UserDTO(
        String id,
        String firstName,
        String lastName,
        String email,
        String number,
        String token,
        List<ClinicModel> clinics,
        List<ClinicUserPrivilegesGroupModel> clinicPrivileges
) {}
