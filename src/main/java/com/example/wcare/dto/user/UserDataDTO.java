package com.example.wcare.dto.user;

import com.example.wcare.data.models.ClinicUserPrivilegesGroupModel;
import lombok.Builder;

@Builder
public record UserDataDTO(
        String id,
        String firstName,
        String lastName,
        String email,
        String number,
        ClinicUserPrivilegesGroupModel group
) {}
