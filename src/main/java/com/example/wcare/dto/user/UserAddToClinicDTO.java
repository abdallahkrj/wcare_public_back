package com.example.wcare.dto.user;

import java.util.List;

public record UserAddToClinicDTO(
        String userId,
        String clinicUserGroupPrivilegesId
) {
}