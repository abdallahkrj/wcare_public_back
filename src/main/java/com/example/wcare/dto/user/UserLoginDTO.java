package com.example.wcare.dto.user;

public record UserLoginDTO(String number, String password) {
}
