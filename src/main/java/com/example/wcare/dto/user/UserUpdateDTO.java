package com.example.wcare.dto.user;

public record UserUpdateDTO(
        String firstName,
        String lastName,
        String email,
        String number,
        String oldPassword,
        String newPassword
) {}
