package com.example.wcare.dto.user;

public record UserChangeGroupDTO(
        String userId,
        String groupId
) {}
