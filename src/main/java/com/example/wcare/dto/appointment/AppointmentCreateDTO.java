package com.example.wcare.dto.appointment;

import com.example.wcare.data.models.*;

import java.time.LocalDateTime;
import java.util.List;

public record AppointmentCreateDTO(
        String title,
        LocalDateTime dateOfReservation,
        List<String> notes,
        double duration,
        String type,
        ClinicInfoModel clinic,
        PatientInfoModel patient,
        UserInfoModel user
) {
}