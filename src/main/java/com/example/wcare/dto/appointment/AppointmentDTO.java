package com.example.wcare.dto.appointment;

import com.example.wcare.data.models.*;
import com.example.wcare.data.object.AppointmentStatus;
import lombok.Builder;

import java.time.LocalDateTime;
import java.util.List;

@Builder
public record AppointmentDTO(
        String id,
        String title,
        LocalDateTime dateOfReservation,
        List<String> notes,
        Double duration,
        SummaryModel summary,
        AppointmentStatus status,
        String type,
        ClinicInfoModel clinic,
        PatientInfoModel patient,
        UserInfoModel user
) {
}
