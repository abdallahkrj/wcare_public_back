package com.example.wcare.dto.appointment;

import com.example.wcare.data.object.AppointmentStatus;
import com.example.wcare.data.object.Summary;
import lombok.*;

//@ToString
//@AllArgsConstructor
//@Setter
@Builder
public record AppointmentUpdateDTO(
        Double duration,
        Summary summary,
        AppointmentStatus status
) {
}
