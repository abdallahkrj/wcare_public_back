package com.example.wcare.service;

import com.example.wcare.dto.privileges.PrivilegesCreateDTO;
import com.example.wcare.dto.privileges.PrivilegesDTO;
import com.example.wcare.data.entities.Privilege;
import com.example.wcare.data.entities.SubscriptionPlan;
import com.example.wcare.repositories.PlanRepo;
import com.example.wcare.repositories.PrivilegesRepo;
import com.example.wcare.util.ClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class PrivilegesService {
    @Autowired
    private PrivilegesRepo privilegesRepo;
    @Autowired
    private PlanRepo planRepo;

    public List<Privilege> getAllPrivilegesByIds(List<String> ids) {
        return privilegesRepo.findAllByIdInAndIsDeletedFalse(ids);
    }

    public List<PrivilegesDTO> getAll() {
        return ClassMapper.INSTANCE.privilegesEntityListToDTOList(privilegesRepo.findAllByIsDeletedFalse());
    }

    public PrivilegesDTO create(PrivilegesCreateDTO privilegesDTO) {
//        try{
        return ClassMapper.INSTANCE.entityToDTO(privilegesRepo.insert(ClassMapper.INSTANCE.DTOtoEntity(privilegesDTO)));
//        }catch(Exception e){
//
//        }
    }

    public List<PrivilegesDTO> getByPlan(String planId) {
        try {
            SubscriptionPlan plan = planRepo.findByIdAndIsDeletedFalse(planId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Plan Not Found"));
            return ClassMapper.INSTANCE.privilegesEntityListToDTOList(plan.getPrivileges());
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
}
