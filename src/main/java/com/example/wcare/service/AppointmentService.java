package com.example.wcare.service;

import com.example.wcare.data.entities.*;
import com.example.wcare.data.iterables.StreamableOffShifts;
import com.example.wcare.data.iterables.StreamableUserShifts;
import com.example.wcare.dto.appointment.AppointmentCreateDTO;
import com.example.wcare.dto.appointment.AppointmentDTO;
import com.example.wcare.dto.appointment.AppointmentUpdateDTO;
import com.example.wcare.data.object.AppointmentStatus;
import com.example.wcare.repositories.*;
import com.example.wcare.util.ClassMapper;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class AppointmentService {
    @Autowired
    private AppointmentRepo appointmentRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ClinicRepo clinicRepo;
    @Autowired
    private PatientRepo patientRepo;
    @Autowired
    private UserShiftRepo shiftsRepo;
    @Autowired
    private ShiftOffRepo offRepo;

    public AppointmentDTO createAppointment(AppointmentCreateDTO appointmentCreateDTO) throws ResponseStatusException {
        ObjectId clinicId = new ObjectId(appointmentCreateDTO.clinic().id());
//        StreamableUserShifts shifts = shiftsRepo.findAllByClinic(clinicId);
//        StreamableOffShifts offList = offRepo.findAllByClinic(clinicId);
//        LocalDateTime time = appointmentCreateDTO.dateOfReservation();
//        if( !shifts.TimeInShift(time) || offList.TimeOff(time)){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Making appointment out of user shift time");
//        }
        Appointment appointment = Appointment.builder()
                .title(appointmentCreateDTO.title())
                .dateOfReservation(appointmentCreateDTO.dateOfReservation())
                .notes(appointmentCreateDTO.notes())
                .duration(appointmentCreateDTO.duration())
                .type(appointmentCreateDTO.type())
                .clinic(Clinic.builder().id(appointmentCreateDTO.clinic().id()).build())
                .patient(Patient.builder().id(appointmentCreateDTO.patient().id()).build())
                .user(User.builder().id(appointmentCreateDTO.user().id()).build())
                .status(AppointmentStatus.PENDING)
                .build();
        appointment = appointmentRepo.insert(appointment);
        return ClassMapper.INSTANCE.entityToDTO(appointment);
    }
    public AppointmentDTO patientCreateAppointment(AppointmentCreateDTO appointmentCreateDTO) throws ResponseStatusException {
        ObjectId clinicId = new ObjectId(appointmentCreateDTO.clinic().id());
        StreamableUserShifts shifts = shiftsRepo.findAllByClinic(clinicId);
        StreamableOffShifts offList = offRepo.findAllByClinic(clinicId);
        LocalDateTime time = appointmentCreateDTO.dateOfReservation();
        if( !shifts.TimeInShift(time) || offList.TimeOff(time)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Making appointment out of user shift time");
        }
        Appointment appointment = Appointment.builder()
                .title(appointmentCreateDTO.title())
                .dateOfReservation(appointmentCreateDTO.dateOfReservation())
                .notes(appointmentCreateDTO.notes())
                .duration(appointmentCreateDTO.duration())
                .type(appointmentCreateDTO.type())
                .clinic(Clinic.builder().id(appointmentCreateDTO.clinic().id()).build())
                .patient(Patient.builder().id(appointmentCreateDTO.patient().id()).build())
                .user(User.builder().id(appointmentCreateDTO.user().id()).build())
                .status(AppointmentStatus.PENDING)
                .build();
        appointment = appointmentRepo.insert(appointment);
        return ClassMapper.INSTANCE.entityToDTO(appointment);
    }
    public AppointmentDTO updateAppointment(AppointmentUpdateDTO appointmentDTO, String id) {
        try {
            Appointment appointment = appointmentRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "appointment not found"));
            if(appointmentDTO.status() != null)
                appointment.setStatus(appointmentDTO.status());
            if(appointmentDTO.duration() != null)
                appointment.setDuration(appointmentDTO.duration());
            if(appointmentDTO.summary() != null)
                appointment.setSummary(appointmentDTO.summary());
            System.out.println("appointment: " + appointment.getSummary());
            System.out.println("dto: " + appointmentDTO.summary());
            appointment = appointmentRepo.save(appointment);
            System.out.println("appointment: " + appointment.getSummary().toString());
            return ClassMapper.INSTANCE.entityToDTO(appointment);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<AppointmentDTO> getAppointmentsByUserAndStatusIn(String userId, List<AppointmentStatus> statuses) throws ResponseStatusException {
        User users = userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "User Not Found"));
        if(statuses == null){
            return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByUserAndIsDeletedFalse(users));
        }
        return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByUserAndStatusInAndIsDeletedFalse(users, statuses));
    }

    public List<AppointmentDTO> getAllByUserInAndClinicInAndStatusIn(List<String> userIds, List<String> clinicIds, List<AppointmentStatus> statuses) throws ResponseStatusException {
        List<Clinic> clinics = clinicRepo.findAllByIdAndIsDeletedFalse(clinicIds);
        if(clinics.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Clinic Not Found");
        }
        List<User> users = userRepo.findAllByIdAndIsDeletedFalse(userIds);
        if(users.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User Not Found");
        }
        if(statuses == null){
            return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.getAllByUserInAndClinicInAndIsDeletedFalse(users, clinics));
        }
        return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.getAllByUserInAndClinicInAndStatusInAndIsDeletedFalse(users, clinics, statuses));
    }

    public List<AppointmentDTO> getAllByClinicAndStatusIn(String clinicId, List<AppointmentStatus> statuses) {
        try {
            Clinic clinic = clinicRepo.findByIdAndIsDeletedFalse(clinicId).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND, "Clinic Not Found"));
            if(statuses == null){
                return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.getAllByClinicAndIsDeletedFalse(clinic));
            }
            return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.getAllByClinicAndStatusInAndIsDeletedFalse(clinic, statuses));
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<AppointmentDTO> getAppointmentsByDate(String userId, List<AppointmentStatus> statuses, LocalDate from, LocalDate to) {
        try {
            User user = userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User Not Found"));
            if (from.isAfter(to)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Date Range is wrong");
            if(statuses == null){
                return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByUserAndDateOfReservationBetweenAndIsDeletedFalse(user, from, to));
            }
            return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByStatusInAndUserAndDateOfReservationBetweenAndIsDeletedFalse(statuses, user, from, to));
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<AppointmentDTO> getAppointmentsByPatientAndDate(ObjectId patient, List<AppointmentStatus> statuses, LocalDate from, LocalDate to) {
        try {
            if (from.isAfter(to)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Date Range is wrong");
            if(statuses == null){
                return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByPatientAndDateOfReservationBetweenAndIsDeletedFalse(patient, from, to));
            }
            return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByStatusInAndPatientAndDateOfReservationBetweenAndIsDeletedFalse(statuses, patient, from, to));
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<AppointmentDTO> getAppointmentsByPatient(ObjectId patient, List<AppointmentStatus> status) {
        if (status == null) {
            return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByPatientAndIsDeletedFalse(patient));
        }
        return ClassMapper.INSTANCE.appointmentEntityListToDTOList(appointmentRepo.findAllByStatusInAndPatientAndIsDeletedFalse(status, patient));
    }
}
