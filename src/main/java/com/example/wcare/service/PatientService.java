package com.example.wcare.service;

import com.example.wcare.auth.JwtUtil;
import com.example.wcare.data.entities.*;
import com.example.wcare.dto.medicalHistory.MedicalHistoryCreateDTO;
import com.example.wcare.dto.medicalHistory.MedicalHistoryDTO;
import com.example.wcare.dto.patient.*;

import com.example.wcare.repositories.*;
import com.example.wcare.util.ClassMapper;
import com.example.wcare.util.MailSender;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Service
public class PatientService {
    private final Environment env;

    @Autowired
    PatientService(Environment env) {
        this.env = env;
    }

    @Autowired
    private PatientRepo patientRepo;
    @Autowired
    private MedicalHistoryRepo medicalHistoryRepo;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private CodeRepo codeRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private AppointmentRepo appointmentRepo;
    @Autowired
    private JwtUtil jwtUtil;
    private final Date authExpirationDate = new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 365));

    public PatientDTO createPatient(PatientSignupDTO patientSignupDTO) throws ResponseStatusException {
        //create patient entity
        Patient patient;
        try {
            patient = patientRepo.insert(ClassMapper.INSTANCE.DTOtoEntity(patientSignupDTO));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Patient Information Not Valid");
        }
        //set token
        String token;
        try {
            //TODO: PatientAuth
            token = jwtUtil.generateToken(patient.getNumber(), patient.getId(), Patient.class, authExpirationDate, new HashMap<>());
        } catch (Exception e) {
            patientRepo.delete(patient);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Token Not Generated Successfully");
        }
        patient.setToken(token);
        patient = patientRepo.save(patient);

        //send email link
        Code code = codeRepo.insert(Code.builder()
                .code(UUID.randomUUID().toString())
                .email(patient.getEmail())
                .build());
        String link = env.getProperty("web.path") + "#/signup/complete?patientId=" + patient.getId() + "&code=" + code.getCode();
        System.out.println("Link is: " + link);
        mailSender.sendMail(patient.getEmail(), "WCare patient complete registration",
                "Hello " + patient.getFirstName() + " " + patient.getLastName() + " !\n"
                        + "Use this link to complete your registration\n"
                        + link
        );
        return ClassMapper.INSTANCE.entityToDTO(patient);

    }

    public PatientDTO completeRegistration(PatientCompleteRegistrationDTO patientCompleteRegistrationDTO) throws ResponseStatusException {
        try {
            Code resetCode = codeRepo.findByCodeAndIsDeletedFalse(patientCompleteRegistrationDTO.code()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "code not found"));
            Patient patient = patientRepo.findByIdAndIsDeletedFalse(patientCompleteRegistrationDTO.patientId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "patient not found"));
            if (!resetCode.getEmail().equals(patient.getEmail())) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "wrong code");
            } else {
                resetCode.setDeleted(true);
                codeRepo.save(resetCode);
            }
            patient.setPassword(patientCompleteRegistrationDTO.password()); //TODO hash
            final String token = jwtUtil.generateToken(patient.getNumber(), patient.getId(), Patient.class, authExpirationDate, new HashMap<>());
            //jwtUtil.setInvalid(patient.getToken()); //TODO
            patient.setToken(token);
            patient = patientRepo.save(patient);
            return ClassMapper.INSTANCE.entityToDTO(patient);
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public PatientDTO login(PatientLoginDTO patientLoginDTO) throws ResponseStatusException {
        try {
            Patient patient = patientRepo.findByNumberAndPasswordAndIsDeletedFalse(patientLoginDTO.number(), patientLoginDTO.password()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Wrong credentials"));
            final String token = jwtUtil.generateToken(patient.getNumber(), patient.getId(), Patient.class, authExpirationDate, new HashMap<>());
            patient.setToken(token);
            return ClassMapper.INSTANCE.entityToDTO(patient);
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<PatientDTO> getAll() throws ResponseStatusException {
        //TODO: pagination
        try {
            return ClassMapper.INSTANCE.patientEntityListToDTOList(patientRepo.findAllByIsDeletedFalse());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public PatientDTO getById(String id) throws ResponseStatusException {
        try {
            return ClassMapper.INSTANCE.entityToDTO(patientRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Patient not found")));
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<MedicalHistoryDTO> getMedicalHistory(String id) throws ResponseStatusException {
        try {
            Patient patient = patientRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "patient Not Found"));
            return ClassMapper.INSTANCE.medicalHistoryEntityListToDTOList(patient.getMedicalHistories());
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public MedicalHistoryDTO addMedicalHistory(String id, MedicalHistoryCreateDTO medicalHistoryDTO) throws ResponseStatusException {
        try {
            Patient patient = patientRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "patient Not Found"));
            FileModel file = null;
            if(medicalHistoryDTO.attachment() != null){
                file = FileModel.builder()
                        .type(medicalHistoryDTO.attachment().type())
                        .file(medicalHistoryDTO.attachment().file())
                        .build();
            }
            MedicalHistory.MedicalHistoryBuilder medicalHistoryBuilder = MedicalHistory.builder()
                    .type(medicalHistoryDTO.type())
                    .description(medicalHistoryDTO.description())
                    .dateTime(medicalHistoryDTO.date())
                    .status(false);

            if(file != null){
                medicalHistoryBuilder.attachment(file);
            }
            MedicalHistory medicalHistory = medicalHistoryRepo.insert(medicalHistoryBuilder.build());
            if (patient.getMedicalHistories() == null) {
                patient.setMedicalHistories(List.of(medicalHistory));
            } else {
                patient.getMedicalHistories().add(medicalHistory);
            }
            patientRepo.save(patient);
            return ClassMapper.INSTANCE.entityToDTO(medicalHistory);
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public MedicalHistoryDTO changeMedicalHistoryStatus(String id, boolean status) {
        try {
            MedicalHistory medicalHistory = medicalHistoryRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "medical history not found"));
            medicalHistory.setStatus(status);
            medicalHistoryRepo.save(medicalHistory);
            return ClassMapper.INSTANCE.entityToDTO(medicalHistory);
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<PatientInfoDTO> getAllByUser(ObjectId userId) throws ResponseStatusException {
        try {
            List<Appointment> appointments = appointmentRepo.findAllByUserAndIsDeletedFalse(userId);
            Map<Clinic, Patient> patientsMap = new HashMap<>();
            appointments.forEach(appointment -> {
                if (patientsMap.entrySet().stream().noneMatch(clinicPatientEntry -> {
                    return clinicPatientEntry.getKey().getId().equals(appointment.getClinic().getId()) && clinicPatientEntry.getValue().getId().equals(appointment.getPatient().getId() );
                })){ //TODO: fix it from repo
                    patientsMap.put(appointment.getClinic(), appointment.getPatient());
                }
            });
            return ClassMapper.INSTANCE.patientEntityMapToInfoDTOList(patientsMap);
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public Object deleteMedicalHistory(String id) {
        try {
            MedicalHistory medicalHistory = medicalHistoryRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "medical history not found"));
            medicalHistory.setDeleted(true);
            medicalHistoryRepo.save(medicalHistory);
            return ClassMapper.INSTANCE.entityToDTO(medicalHistory);
        } catch (ResponseStatusException re) {
            throw re;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
}
