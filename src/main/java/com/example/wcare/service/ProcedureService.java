package com.example.wcare.service;

import com.example.wcare.data.entities.*;
import com.example.wcare.data.object.*;
import com.example.wcare.dto.procedures.DentalProceduresCreateDTO;
import com.example.wcare.dto.procedures.ProcedureDataDTO;
import com.example.wcare.dto.procedures.ProcedureMetaDataDTO;
import com.example.wcare.dto.procedures.ProceduresByToothDTO;
import com.example.wcare.repositories.*;
import com.example.wcare.util.ClassMapper;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class ProcedureService {

    @Autowired
    private ProcedureRepo procedureRepo;
    @Autowired
    private FileRepo fileRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ClinicRepo clinicRepo;
    @Autowired
    private PatientRepo patientRepo;
    @Autowired
    private UserProcedureTypesRepo userProcedureTypesRepo;
    @Autowired
    private SpecialityTypesRepo specialityTypesRepo;

    public ProcedureMetaDataDTO createDentalProcedure(DentalProceduresCreateDTO dentalProceduresCreateDTO, String clinicId) throws ResponseStatusException {
        try {
            User user = userRepo.findByIdAndIsDeletedFalse(dentalProceduresCreateDTO.userId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            Clinic clinic = clinicRepo.findByIdAndIsDeletedFalse(clinicId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic not found"));
            Patient patient = patientRepo.findByIdAndIsDeletedFalse(dentalProceduresCreateDTO.patientId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "patient not found"));
            //TODO: Validation for Ids given in api
            Procedure procedure = Procedure.builder()
                    .mainTypeId(dentalProceduresCreateDTO.mainTypeId())
                    .subTypeId(dentalProceduresCreateDTO.subTypeId())
                    .user(user)
                    .patient(patient)
                    .speciality(Specialities.DENTAL)
                    .clinic(clinic)
                    .procedureDate(dentalProceduresCreateDTO.procedureDate())
                    .details(dentalProceduresCreateDTO.details())
                    .build();
            procedure = procedureRepo.insert(procedure);
            return ClassMapper.INSTANCE.entityToDTO(procedure);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<ProcedureMetaDataDTO> getAllByPatientAndClinicAndType(ObjectId patient, ObjectId clinic, Specialities speciality) {
        try {
            List<Procedure> procedures = procedureRepo.findAllByPatientAndClinicAndIsDeletedFalse(patient, clinic);
            procedures.removeIf(procedure -> !procedure.getUser().getSpeciality().equals(speciality));
            return procedures.stream().map(procedure -> {
                return ClassMapper.INSTANCE.entityToDTO(procedure);
            }).toList();
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<ProceduresByToothDTO> getAllToothFileByPatientAndClinicAndTypeByTooth(ObjectId patientId, ObjectId clinicId) {
        try {
            List<ProcedureMetaDataDTO> procedures = getAllByPatientAndClinicAndType(patientId, clinicId, Specialities.DENTAL);
            Map<Integer, List<ProcedureMetaDataDTO>> proceduresByToothDTOMap = new HashMap<>();
            procedures.forEach(
                    procedure -> {
                        if(!procedure.mainType().equals("toothFile")){
                            return;
                        }
                        int toothNumber = ((DentalToothFile) procedure.details()).getToothNumber();
                        if (proceduresByToothDTOMap.containsKey(toothNumber)) {
                            proceduresByToothDTOMap.get(toothNumber).add(procedure);
                        } else {
                            LinkedList<ProcedureMetaDataDTO> newList = new LinkedList<>();
                            newList.add(procedure);
                            proceduresByToothDTOMap.put(toothNumber, newList);
                        }
                    }
            );
            List<ProceduresByToothDTO> proceduresByToothDTOList = new LinkedList<>();
            proceduresByToothDTOMap.forEach((toothNumber, procedureMetaDataDTOS) ->
                    proceduresByToothDTOList.add(ProceduresByToothDTO
                            .builder()
                            .toothNumber(toothNumber)
                            .procedureList(procedureMetaDataDTOS)
                            .build())
            );
            return proceduresByToothDTOList;
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
    public List<ProceduresByToothDTO> getAllByPatientAndClinicAndTypeByTooth(ObjectId patientId, ObjectId clinicId) {
        try {
            List<ProcedureMetaDataDTO> procedures = getAllByPatientAndClinicAndType(patientId, clinicId, Specialities.DENTAL);

            Map<Integer, List<ProcedureMetaDataDTO>> proceduresByToothDTOMap = new HashMap<>();
            procedures.forEach(
                    procedure -> {
                        if(procedure.details().getClass() != DentalProcedure.class){
                            return;
                        }
                        int toothNumber = ((DentalProcedure) procedure.details()).getToothNumber();
                        if (proceduresByToothDTOMap.containsKey(toothNumber)) {
                            proceduresByToothDTOMap.get(toothNumber).add(procedure);
                        } else {
                            LinkedList<ProcedureMetaDataDTO> newList = new LinkedList<>();
                            newList.add(procedure);
                            proceduresByToothDTOMap.put(toothNumber, newList);
                        }
                    }
            );
            List<ProceduresByToothDTO> proceduresByToothDTOList = new LinkedList<>();
            proceduresByToothDTOMap.forEach((toothNumber, procedureMetaDataDTOS) ->
                    proceduresByToothDTOList.add(ProceduresByToothDTO
                            .builder()
                            .toothNumber(toothNumber)
                            .procedureList(procedureMetaDataDTOS)
                            .build())
            );
            return proceduresByToothDTOList;
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<ProcedureMetaDataDTO> getAllFilesByPatientAndClinicAndType(ObjectId patientId, ObjectId clinicId) {
        try {
            return getAllByPatientAndClinicAndType(patientId, clinicId, Specialities.DENTAL).stream().filter(procedureMetaDataDTO -> procedureMetaDataDTO.mainType().equals("file")).toList();
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public ProcedureMetaDataDTO addFile(String patientId, String userId, byte[] fileBytes, String clinicId) {
        try {
            Patient patient = patientRepo.findByIdAndIsDeletedFalse(patientId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "patient not found"));
            User user = userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            Clinic clinic = clinicRepo.findByIdAndIsDeletedFalse(clinicId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic not found"));

            SpecialityTypes specialityTypes =  specialityTypesRepo.findBySpecialityAndIsDeletedFalse(Specialities.DENTAL).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
            Optional<TypeObject> type = specialityTypes.getMainTypes().stream().filter(typeObject -> typeObject.type().equals("file")).findFirst();
            UUID mainTypeId;
            if(type.isPresent()){
                mainTypeId = type.get().id();
            }else{
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "mainType:file not found");
            }
            FileModel fileEntity = FileModel.builder().type("image").file(fileBytes).build();
            fileEntity = fileRepo.insert(fileEntity);
            Procedure procedure = Procedure.builder()
                    .mainTypeId(mainTypeId)
                    .user(user)
                    .speciality(Specialities.DENTAL)
                    .patient(patient)
                    .clinic(clinic)
                    .procedureDate(LocalDateTime.now())
                    .details(new DentalFile(new com.example.wcare.data.models.FileModel(fileEntity.getId(), fileEntity.getType())))
                    .build();
            procedure = procedureRepo.insert(procedure);
            return ClassMapper.INSTANCE.entityToDTO(procedure);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
    public ProcedureMetaDataDTO addToothFile(short toothNumber, String patientId, String userId, byte[] bytes, String clinicId) {
        try {
            Patient patient = patientRepo.findByIdAndIsDeletedFalse(patientId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "patient not found"));
            User user = userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            Clinic clinic = clinicRepo.findByIdAndIsDeletedFalse(clinicId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic not found"));

            SpecialityTypes specialityTypes =  specialityTypesRepo.findBySpecialityAndIsDeletedFalse(Specialities.DENTAL).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
            Optional<TypeObject> type = specialityTypes.getMainTypes().stream().filter(typeObject -> typeObject.type().equals("toothFile")).findFirst();
            UUID mainTypeId;
            if(type.isPresent()){
                mainTypeId = type.get().id();
            }else{
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "mainType:file not found");
            }
            FileModel fileEntity = FileModel.builder().type("image").file(bytes).build();
            fileEntity = fileRepo.insert(fileEntity);
            Procedure procedure = Procedure.builder()
                    .mainTypeId(mainTypeId)
                    .user(user)
                    .patient(patient)
                    .speciality(Specialities.DENTAL)
                    .clinic(clinic)
                    .procedureDate(LocalDateTime.now())
                    .details(new DentalToothFile(toothNumber, new com.example.wcare.data.models.FileModel(fileEntity.getId(), fileEntity.getType())))
                    .build();
            return ClassMapper.INSTANCE.entityToDTO(procedureRepo.save(procedure));
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<ProcedureDataDTO> getAllByPatientAndType(ObjectId patientId, Specialities speciality) {
        try {
            List<Procedure> procedures = procedureRepo.findAllByPatientAndSpecialityAndIsDeletedFalse(patientId, speciality);
            return ClassMapper.INSTANCE.entityToDTO2List(procedures);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

//    public List<ProcedureDataDTO> getAllFilesByPatient(ObjectId id) {
//        List<Procedure> procedures = procedureRepo.findAllByPatientAndSpecialityAndIsDeletedFalse(id, Specialities.DENTAL);
//        SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(Specialities.DENTAL).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality not found"));
//        UUID filesUUID = specialityTypes.getTypeUUID("files");
//        procedures.removeIf(procedure -> !procedure.getMainTypeId().equals(filesUUID));
//        return ClassMapper.INSTANCE.entityToDTO2List(procedures);
//    }
//
//    public List<ProceduresByToothDTO> getAllToothFileByPatientByTooth(ObjectId id) {
//        Map<Short, List<Procedure>> procedures = procedureRepo.findAllByPatientAndSpecialityAndIsDeletedFalseGroupByDetailsToothNumber(id, Specialities.DENTAL);
//        SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(Specialities.DENTAL).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality not found"));
//        UUID toothFilesUUID = specialityTypes.getTypeUUID("toothFile");
//        List<ProceduresByToothDTO> proceduresByToothDTOList = new LinkedList<>();
//        procedures.forEach((aShort, proceduresList) -> {
//            proceduresList.removeIf(procedure -> !procedure.getMainTypeId().equals(toothFilesUUID));
//            proceduresByToothDTOList.add(ProceduresByToothDTO.builder()
//                            .toothNumber(aShort)
//                            .procedureList(ClassMapper.INSTANCE.entityToDTOList(proceduresList))
//                    .build());
//        });
//        return proceduresByToothDTOList;
//    }
}
