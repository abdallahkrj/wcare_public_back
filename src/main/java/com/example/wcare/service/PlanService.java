package com.example.wcare.service;

import com.example.wcare.dto.subscriptionPlan.SubscriptionPlanCreateDTO;
import com.example.wcare.dto.subscriptionPlan.SubscriptionPlanDTO;
import com.example.wcare.data.entities.SubscriptionPlan;
import com.example.wcare.repositories.PlanRepo;
import com.example.wcare.util.ClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class PlanService {

    @Autowired
    private PlanRepo planRepo;

    public SubscriptionPlanDTO createPlan(SubscriptionPlanCreateDTO planDTO) throws ResponseStatusException {
            try {
                SubscriptionPlan entity = ClassMapper.INSTANCE.DTOtoEntity(planDTO);
                System.out.print(entity);
                return ClassMapper.INSTANCE.entityToDTO(planRepo.insert(entity));
            } catch (Exception e) {
                e.printStackTrace();
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
            }
    }

    public List<SubscriptionPlanDTO> getAllPlans() {
        try {
            return ClassMapper.INSTANCE.planEntityListToDTOList(planRepo.findAllByIsDeletedFalse());
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
}
