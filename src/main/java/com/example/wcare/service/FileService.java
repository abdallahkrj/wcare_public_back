package com.example.wcare.service;

import com.example.wcare.data.entities.FileModel;
import com.example.wcare.repositories.FileRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class FileService {

    @Autowired
    private FileRepo fileRepo;

    public Resource getById(String id) {
        try {
            FileModel file = fileRepo.findByIdAndIsDeletedFalse(id);
            return new ByteArrayResource(file.getFile());
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
}
