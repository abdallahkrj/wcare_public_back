package com.example.wcare.service;

import com.example.wcare.dto.clinicUserGroupPrivileges.ClinicUserGroupPrivilegesCreateDTO;
import com.example.wcare.dto.clinicUserGroupPrivileges.ClinicUserGroupPrivilegesDTO;
import com.example.wcare.data.entities.Clinic;
import com.example.wcare.data.entities.ClinicRoleGroup;
import com.example.wcare.data.entities.Privilege;
import com.example.wcare.data.entities.User;
import com.example.wcare.repositories.ClinicRoleGroupRepo;
import com.example.wcare.repositories.UserRepo;
import com.example.wcare.util.ClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class ClinicRoleGroupService {
    @Autowired
    ClinicRoleGroupRepo clinicRoleGroupRepo;
    @Autowired
    ClinicService clinicService;
    @Autowired
    UserRepo userRepo;
    @Autowired
    PrivilegesService privilegesService;

    public ClinicUserGroupPrivilegesDTO createClinicUserPrivileges(ClinicUserGroupPrivilegesCreateDTO createDTO, String userNumber, String clinicId) throws ResponseStatusException {
        try {
            Clinic clinic = clinicService.getById(clinicId);
            // get the user who need to create new user and his privileges in the clinic he need to create user in
            User callerUser = userRepo.findByNumberAndIsDeletedFalse(userNumber).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
            List<ClinicRoleGroup> clinicUserGroupPrivileges = callerUser.getClinicPrivileges().stream().filter(clinicUserPrivilege -> clinicUserPrivilege.getClinic().equals(clinic)).toList();
            // the user should have a privileges relation with this clinic
            if (clinicUserGroupPrivileges.isEmpty()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User can't create new user in this clinic");
            }
            // the user should only have one privileges relation with this clinic or there is something wrong in the database
            if (clinicUserGroupPrivileges.size() > 1) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "User has conflict privileges in this clinic.. please check system admin to fix this issue");
            }
            // create the privileges relation with the clinic he belongs to
            ClinicRoleGroup userClinicPrivileges = clinicUserGroupPrivileges.get(0);
            List<Privilege> privileges = privilegesService.getAllPrivilegesByIds(createDTO.privilegesIds());
            // remove the privileges that was given from the creator user and not in his privileges on this clinic
            privileges.removeIf(privilege -> !userClinicPrivileges.getPrivileges().contains(privilege));

            List<Privilege> planPrivileges = clinic.plan().getPrivileges();
            privileges.removeIf(privilege -> !planPrivileges.contains(privilege));
            ClinicRoleGroup clinicUserGroupPrivileges1 = clinicRoleGroupRepo.insert(ClinicRoleGroup.builder().name(createDTO.name()).privileges(privileges).clinic(clinic).build());
            System.out.println("clinicPrivilege: " + clinicUserGroupPrivileges1);
            return ClassMapper.INSTANCE.entityToDTO(clinicUserGroupPrivileges1);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }

    public ClinicRoleGroup addUserToClinicUserGroupPrivileges(String clinicUserGroupPrivilegesId, Clinic clinic, User user) {
        try {
            ClinicRoleGroup clinicUserGroupPrivileges = clinicRoleGroupRepo.findByIdAndIsDeletedFalse(clinicUserGroupPrivilegesId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic user group privileges not found"));
            if (!clinicUserGroupPrivileges.getClinic().equals(clinic))
                throw new ResponseStatusException(HttpStatus.CONFLICT, "group privileges does not belong to the clinic");
            clinicUserGroupPrivileges.getUsers().add(user);
            return clinicRoleGroupRepo.save(clinicUserGroupPrivileges);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    public ClinicRoleGroup getById(String id) throws Exception {
        return clinicRoleGroupRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic user group privileges not found"));
    }

    public void changeUserGroup(User user, ClinicRoleGroup group, Clinic clinic) {
        ClinicRoleGroup oldGroup = clinicRoleGroupRepo.findByClinicAndUsersAndIsDeletedFalse(clinic, user).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic user group privileges not found"));
        oldGroup.getUsers().remove(user);
        group.getUsers().add(user);
        System.out.println(oldGroup);
        System.out.println(group.getUsers());
        System.out.println("new group" + group);
        List<ClinicRoleGroup> clinicUserGroupPrivileges = clinicRoleGroupRepo.saveAll(List.of(oldGroup, group));
        System.out.println(clinicUserGroupPrivileges.get(0).getUsers().size());
        System.out.println(clinicUserGroupPrivileges);
    }

    public ClinicUserGroupPrivilegesDTO adminCreateClinicUserPrivileges(ClinicUserGroupPrivilegesCreateDTO createDTO) throws ResponseStatusException {
        try {
            Clinic clinic = clinicService.getById(createDTO.clinicId());
            List<Privilege> privileges = privilegesService.getAllPrivilegesByIds(createDTO.privilegesIds());

            List<Privilege> planPrivileges = clinic.plan().getPrivileges();
            privileges.removeIf(privilege -> !planPrivileges.contains(privilege));
            ClinicRoleGroup clinicUserGroupPrivileges1 = clinicRoleGroupRepo.insert(ClinicRoleGroup.builder().name(createDTO.name()).privileges(privileges).clinic(clinic).build());
            return ClassMapper.INSTANCE.entityToDTO(clinicUserGroupPrivileges1);
//            return ClassMapper.INSTANCE.entityToDTO(clinicUserPrivilegesGroupRepo.findById(clinicUserGroupPrivileges1.getId()).orElseThrow());
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }

    public List<ClinicUserGroupPrivilegesDTO> getAllByClinic(String clinicId) {
        try{
            Clinic clinic = clinicService.getById(clinicId);
            return ClassMapper.INSTANCE.clinicUserGroupPrivilegesEntityListToDTOs(clinicRoleGroupRepo.findAllByClinicAndIsDeletedFalse(clinic));
        }catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }

    public ClinicUserGroupPrivilegesDTO updateGroup(String id, List<String> privilegesIds) {
        try{
            ClinicRoleGroup clinicUserGroupPrivileges = clinicRoleGroupRepo.findByIdAndIsDeletedFalse(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic user group privileges"));
            List<Privilege> privileges = privilegesService.getAllPrivilegesByIds(privilegesIds);
            clinicUserGroupPrivileges.setPrivileges(privileges);
            return ClassMapper.INSTANCE.entityToDTO(clinicRoleGroupRepo.save(clinicUserGroupPrivileges));
        }catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }

    public boolean deleteGroup(String id) {
        try{
            ClinicRoleGroup clinicUserGroupPrivileges = clinicRoleGroupRepo.findByIdAndIsDeletedFalse(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "clinic user group privileges"));
            clinicUserGroupPrivileges.setDeleted(true);
            clinicRoleGroupRepo.save(clinicUserGroupPrivileges);
            return true;
        }catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }

    public List<ClinicUserGroupPrivilegesDTO> getAllByUser(String userId) {
        try{
            User user = userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            return ClassMapper.INSTANCE.clinicUserGroupPrivilegesEntityListToDTOs(clinicRoleGroupRepo.findAllByUsersAndIsDeletedFalse(user));
        }catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }
}
