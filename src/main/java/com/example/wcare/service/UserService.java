package com.example.wcare.service;

import com.example.wcare.auth.JwtUtil;
import com.example.wcare.data.entities.*;
import com.example.wcare.dto.user.*;

import com.example.wcare.data.entities.Code.CodeBuilder;
import com.example.wcare.repositories.CodeRepo;
import com.example.wcare.repositories.UserRepo;
import com.example.wcare.util.ClassMapper;
import com.example.wcare.util.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Service
public class UserService {
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ClinicService clinicService;
    @Autowired
    private PrivilegesService privilegesService;
    @Autowired
    private CodeRepo codeRepo;
    @Autowired
    private MailSender mailSender;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private ClinicRoleGroupService clinicRoleGroupService;
    private final Date authExpirationDate = new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 365));

    public UserDTO login(UserLoginDTO userLoginDTO) throws ResponseStatusException {
        try {
            User user = userRepo.findByNumberAndPasswordAndIsDeletedFalse(
                            userLoginDTO.number(),
                            userLoginDTO.password())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
            final String token = jwtUtil.generateToken(user.getNumber(), user.getId(), User.class, authExpirationDate, new HashMap<>());
            user.setToken(token);
            return ClassMapper.INSTANCE.
                    entityToDTO(user);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public UserDTO create(UserSignupDTO userSignupDTO, String userNumber, String clinicId) throws ResponseStatusException {
        User user;
        try {
            Clinic clinic = clinicService.getById(clinicId);
            //create the new user
            user = User.builder().email(userSignupDTO.email()).firstName(userSignupDTO.firstName()).lastName(userSignupDTO.lastName()).number(userSignupDTO.number()).password(userSignupDTO.password()).clinics(List.of(clinic)).build();
            user = userRepo.insert(user);
            ClinicRoleGroup clinicUserGroupPrivileges = clinicRoleGroupService.getById(userSignupDTO.clinicUserGroupPrivilegesId());
            clinicRoleGroupService.addUserToClinicUserGroupPrivileges(clinicUserGroupPrivileges.getId(), clinic, user);
        } catch (ResponseStatusException e) {
            // delete user created if not null
            throw e;
        } catch (Exception e) {
            // delete user created if not null
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User Information Not Valid");
        }
        String token;
        try {
            //TODO: UserAuth
            token = jwtUtil.generateToken(user.getNumber(), user.getId(), User.class, authExpirationDate, new HashMap<>());
        } catch (Exception e) {
            userRepo.delete(user);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Token Not Generated Successfully");
        }
        mailSender.sendMail(user.getEmail(), "WCare User complete registration.",
                "Hello " + user.getFirstName() + " " + user.getLastName() + " !\n"
                        + "Your Account Details Are:"
//                        + "Phone Number:" + user.getNumber() + ".\n"
                        + "Password:" + user.getPassword()  + ".\n"
                        + "\n"
                        + "\nMake sure to change your account password for your security."
        );
        user.setToken(token);
        user = userRepo.save(user);
        return ClassMapper.INSTANCE.entityToDTO(user);
    }


    public UserDTO addUserToClinic(UserAddToClinicDTO userAddToClinicDTO, String clinicId) {
        try {
            User user = userRepo.findByIdAndIsDeletedFalse(userAddToClinicDTO.userId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            Clinic clinic = clinicService.getById(clinicId);
            user.getClinics().add(clinic);
            userRepo.save(user);
            ClinicRoleGroup groupPrivileges = clinicRoleGroupService.addUserToClinicUserGroupPrivileges(userAddToClinicDTO.clinicUserGroupPrivilegesId(), clinic, user);
            return ClassMapper.INSTANCE.entityToDTO(userRepo.findByIdAndIsDeletedFalse(userAddToClinicDTO.userId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found")));
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public void forgetPasswordDemand(String email) {
        try {
            User user = userRepo.findByEmailAndIsDeletedFalse(email).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
            CodeBuilder builder = Code.builder();
            builder.code(UUID.randomUUID().toString().substring(0, 6));
            builder.email(user.getEmail());
            Code passwordResetCode = builder.build();
            Code newCode = codeRepo.insert(passwordResetCode);
            String code = newCode.getCode();
            mailSender.sendMail(user.getEmail(), "reset your password",
                    "Hello " + user.getFirstName() + " " + user.getLastName() + " !\n we believe you requested to change your password.\n"
                            + "Use this code to reset your password\n"
                            + code
            );
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<UserDTO> getAll() {
        try {
            return ClassMapper.INSTANCE.userEntityListToDTOList(userRepo.findAllByIsDeletedFalse());
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<UserDataDTO> getAllByClinic(String clinicId, String privilege) {
        try {
            Clinic clinic = clinicService.getById(clinicId);
            List<User> users = userRepo.findAllByClinicsAndIsDeletedFalse(clinic);
            if (privilege == null) {
                return ClassMapper.INSTANCE.userEntityListToDataDTOList(users, clinicId);
            }
            //user don't have privilege in clinic
            users.removeIf(user ->
                    user.getClinicPrivileges().stream().filter(
                            clinicUserGroupPrivileges ->
                                    Objects.equals(clinicUserGroupPrivileges.getClinic().getId(), clinicId)
                    ).findAny().orElseThrow().getPrivileges().stream().noneMatch(privileges ->
                            privileges.getName().equals(privilege)
                    )
            );
            return ClassMapper.INSTANCE.userEntityListToDataDTOList(users, clinicId);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
    public List<UserDTO> getAllByNotInClinic(String clinicId, String privilege) {
        try {
            List<User> users = userRepo.findAllByIsDeletedFalse();
            users.removeIf(user -> user.getClinics().stream().anyMatch(clinic -> clinic.getId().equals(clinicId)));
            if (privilege == null) {
                return ClassMapper.INSTANCE.userEntityListToDTOList(users);
            }
            //user don't have privilege in clinic
            users.removeIf(user ->
                    user.getClinicPrivileges().stream().filter(
                            clinicUserGroupPrivileges ->
                                    Objects.equals(clinicUserGroupPrivileges.getClinic().getId(), clinicId)
                    ).findAny().orElseThrow().getPrivileges().stream().noneMatch(privileges ->
                            privileges.getName().equals(privilege)
                    )
            );
            return ClassMapper.INSTANCE.userEntityListToDTOList(users);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public void checkCode(String code, String email) {
        try {
            Code resetCode = codeRepo.findByCodeAndIsDeletedFalse(code).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "code not found"));
            if (!resetCode.getEmail().equals(email)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "wrong code");
            }
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public UserDTO updatePassword(String email, String code, String password) {
        try {
            Code resetCode = codeRepo.findByCodeAndIsDeletedFalse(code).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "code not found"));
            if (!resetCode.getEmail().equals(email)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "wrong code");
            } else {
                resetCode.setDeleted(true);
                codeRepo.save(resetCode);
            }
            User user = userRepo.findByEmailAndIsDeletedFalse(email)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
            user.setPassword(password);
            return ClassMapper.INSTANCE.entityToDTO(user);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public UserDTO adminCreate(UserSignupDTO userSignupDTO, String clinicId) throws ResponseStatusException {
        User user;
        try {
            Clinic clinic = clinicService.getById(clinicId);
            user = User.builder().speciality(userSignupDTO.speciality()).email(userSignupDTO.email()).firstName(userSignupDTO.firstName()).lastName(userSignupDTO.lastName()).number(userSignupDTO.number()).password(userSignupDTO.password()).clinics(List.of(clinic)).build();
            ClinicRoleGroup clinicUserGroupPrivileges = clinicRoleGroupService.getById(userSignupDTO.clinicUserGroupPrivilegesId());
            user = userRepo.insert(user);
            clinicRoleGroupService.addUserToClinicUserGroupPrivileges(clinicUserGroupPrivileges.getId(), clinic, user);
        } catch (ResponseStatusException e) {
            // delete user created if not null
            throw e;
        } catch (Exception e) {
            // delete user created if not null
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User Information Not Valid");
        }
        String token;
        try {
            //TODO: UserAuth
            token = jwtUtil.generateToken(user.getNumber(),user.getId(), User.class, authExpirationDate, new HashMap<>());
        } catch (Exception e) {
            userRepo.delete(user);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Token Not Generated Successfully");
        }
        user.setToken(token);
        user = userRepo.save(user);
        return ClassMapper.INSTANCE.entityToDTO(user);
    }

    public UserDTO updateUser(UserUpdateDTO userUpdateDTO, String id) throws ResponseStatusException {
        User user;
        try {
            user = userRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            if (userUpdateDTO.firstName() != null) {
                user.setEmail(userUpdateDTO.firstName());
            }
            if (userUpdateDTO.lastName() != null) {
                user.setEmail(userUpdateDTO.lastName());
            }
            if (userUpdateDTO.number() != null) {
                user.setEmail(userUpdateDTO.number());
            }
            if (userUpdateDTO.newPassword() != null && userUpdateDTO.oldPassword().equals(user.getPassword())) {
                user.setPassword(userUpdateDTO.newPassword());
            }
            if (userUpdateDTO.email() != null) {
                user.setEmail(userUpdateDTO.email());
            }
        } catch (ResponseStatusException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
        String token;
        try {
            //TODO: UserAuth
            token = jwtUtil.generateToken(user.getNumber(),user.getId(), User.class, authExpirationDate, new HashMap<>());
        } catch (Exception e) {
            userRepo.delete(user);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Token Not Generated Successfully");
        }
        user.setToken(token);
        user = userRepo.save(user);
        return ClassMapper.INSTANCE.entityToDTO(user);
    }


    public UserDTO changeGroup(UserChangeGroupDTO userChangeGroupDTO, String clinicId) {
        try {
            User user = userRepo.findByIdAndIsDeletedFalse(userChangeGroupDTO.userId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            ClinicRoleGroup group = clinicRoleGroupService.getById(userChangeGroupDTO.groupId());
            Clinic clinic = clinicService.getById(clinicId);
            clinicRoleGroupService.changeUserGroup(user, group, clinic);
            return ClassMapper.INSTANCE.entityToDTO(userRepo.findById(userChangeGroupDTO.userId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found")));
        } catch (ResponseStatusException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "internal server error");
        }
    }

    public User getById(String userId) throws ResponseStatusException {
        return userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
    }
}
