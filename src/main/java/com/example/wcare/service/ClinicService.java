package com.example.wcare.service;

import com.example.wcare.data.entities.ShiftOff;
import com.example.wcare.data.entities.User;
import com.example.wcare.data.entities.UserShift;
import com.example.wcare.data.iterables.StreamableOffShifts;
import com.example.wcare.data.iterables.StreamableUserShifts;
import com.example.wcare.dto.clinic.*;
import com.example.wcare.data.entities.Clinic;
import com.example.wcare.repositories.ClinicRepo;
import com.example.wcare.repositories.UserRepo;
import com.example.wcare.repositories.UserShiftRepo;
import com.example.wcare.repositories.ShiftOffRepo;
import com.example.wcare.util.ClassMapper;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;


@Service
public class ClinicService {
    @Autowired
    ClinicRepo clinicRepo;
    @Autowired
    UserShiftRepo shiftsRepo;
    @Autowired
    ShiftOffRepo offRepo;
    @Autowired
    UserRepo userRepo;


    public List<ClinicDTO> getAll() {
        return ClassMapper.INSTANCE.clinicEntityListToDTOList(clinicRepo.findAllByIsDeletedFalse());
    }

    public ClinicDTO create(ClinicDTO clinicDTO) throws ResponseStatusException {
        try {
            return ClassMapper.INSTANCE.entityToDTO(clinicRepo.insert(ClassMapper.INSTANCE.DTOtoEntity(clinicDTO)));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Clinic Information Not Valid");
        }
    }
    public ClinicDTO update(ClinicUpdateDTO clinicUpdateDTO, String clinicId) throws ResponseStatusException {
        try {
            Clinic clinic = clinicRepo.findByIdAndIsDeletedFalse(clinicId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Clinic Not Found"));
            if (clinicUpdateDTO.name() != null) {
                clinic.setName(clinicUpdateDTO.name());
            }
            if (clinicUpdateDTO.address() != null) {
                clinic.setAddress(clinicUpdateDTO.address());
            }
            if (clinicUpdateDTO.number() != null) {
                clinic.setNumber(clinicUpdateDTO.number());
            }
            if (clinicUpdateDTO.userColors() != null) {
                clinic.setUserColors(clinicUpdateDTO.userColors());
            }
            if (clinicUpdateDTO.settings() != null) {
                clinic.setSettings(clinicUpdateDTO.settings());
            }
            if (clinicUpdateDTO.specialties() != null) {
                clinic.setSpecialties(clinicUpdateDTO.specialties());
            }
            if (clinicUpdateDTO.files() != null) {
                clinic.setFiles(ClassMapper.INSTANCE.fileModelListToEntityList(clinicUpdateDTO.files()));
            }
            return ClassMapper.INSTANCE.entityToDTO(clinicRepo.save(clinic));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Clinic Information Not Valid");
        }
    }

    public Clinic getById(String id) {
        return clinicRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Clinic not found"));
    }

    public ShiftsDTO getShifts(String clinicId) {
        ObjectId clinic = new ObjectId(clinicId);
        return ShiftsDTO.builder()
                .clinicId(clinicId)
                .offList(ClassMapper.INSTANCE.shiftOffToDTO(offRepo.findAllByClinic(clinic).toList()))
                .userShifts(ClassMapper.INSTANCE.userShiftToDTO(shiftsRepo.findAllByClinic(clinic).toList()))
                .build();
    }

    public UserShiftDTO putShift(UserShiftDTO userShiftDTO) {
        UserShift userShift = ClassMapper.INSTANCE.dtoToEntity(userShiftDTO);
        return ClassMapper.INSTANCE.entityToDTO(shiftsRepo.save(userShift));
    }

    public Map<String, String> deleteShift(String id) {
        shiftsRepo.deleteById(id);
        return Map.of("message", "Shift deleted successfully");
    }

    public ShiftOffDTO addShiftOff(ShiftOffDTO shiftOffDTO) {
        ShiftOff shiftOff = ClassMapper.INSTANCE.dtoToEntity(shiftOffDTO);
        return ClassMapper.INSTANCE.entityToDTO(offRepo.save(shiftOff));
    }

    public Map<String, String> deleteShiftOff(String id) {
        offRepo.deleteById(id);
        return Map.of("message", "Shift-off deleted successfully");
    }

    public List<ClinicUserDTO> getUsers(String clinicId) {
        //TODO check if can be replaced with custom query
        List<User> users = userRepo.findAllByClinicsAndIsDeletedFalse(Clinic.builder().id(clinicId).build());
        StreamableUserShifts shifts = shiftsRepo.findAllByClinic(new ObjectId(clinicId));
        StreamableOffShifts offs = offRepo.findAllByClinic(new ObjectId(clinicId));

        return users.stream().map(
                user -> {
                    final UserShift shift = shifts.stream().filter(userShift -> userShift.getUser().getId().equals(user.getId())).findAny().orElse(null);
                    final List<ShiftOff> offList = offs.stream().filter(shiftOff -> shiftOff.getUser().getId().equals(user.getId())).toList();
                    return ClassMapper.INSTANCE.toDTO(user, clinicId, shift, offList);
                }
        ).toList();
    }


}
