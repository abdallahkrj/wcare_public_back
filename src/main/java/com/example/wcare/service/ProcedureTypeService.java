package com.example.wcare.service;

import com.example.wcare.data.entities.SpecialityTypes;
import com.example.wcare.data.entities.User;
import com.example.wcare.data.entities.UserProcedureTypes;
import com.example.wcare.dto.procedures.*;

import com.example.wcare.data.object.Specialities;
import com.example.wcare.data.object.SubTypeObject;
import com.example.wcare.data.object.TypeObject;
import com.example.wcare.repositories.*;
import com.example.wcare.util.ClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Service
public class ProcedureTypeService {

    @Autowired
    private UserProcedureTypesRepo userProcedureTypesRepo;
    @Autowired
    private SpecialityTypesRepo specialityTypesRepo;
    @Autowired
    private UserRepo userRepo;


    public ProcedureSubTypeDTO addSubType(ProcedureSubTypeCreateDTO procedureSubTypeCreateDTO) throws ResponseStatusException {
        try {

            User user = userRepo.findByIdAndIsDeletedFalse(procedureSubTypeCreateDTO.userId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            //checking mainId
            SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(user.getSpeciality()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
            TypeObject mainType = specialityTypes.getMainTypes().stream().filter(typeObject -> typeObject.id().equals(procedureSubTypeCreateDTO.mainTypeId())).findAny().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "main Type not found"));

            //get UserProcedureTypes or create if not found
            UserProcedureTypes userProcedureTypes = userProcedureTypesRepo.findByUserAndIsDeletedFalse(user).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User procedure types not found"));

            //create TypeObject
            UUID uuid = UUID.randomUUID();
            TypeObject typeObject = TypeObject.builder().id(uuid).type(procedureSubTypeCreateDTO.subType()).build();
            //create SubTypeObject if is not found
            Optional<SubTypeObject> subType = userProcedureTypes.getSubTypes().stream().filter(subTypeObject -> subTypeObject.mainId().equals(procedureSubTypeCreateDTO.mainTypeId())).findAny();
            if (subType.isPresent()) {
                subType.get().subTypes().add(typeObject);
            }
            //add to SubTypeObject if found
            else {
                SubTypeObject subTypeObject = SubTypeObject.builder().mainId(procedureSubTypeCreateDTO.mainTypeId()).subTypes(new HashSet<>() {{
                    add(typeObject);
                }}).build();
                userProcedureTypes.getSubTypes().add(subTypeObject);
            }
            System.out.println(userProcedureTypes);
            userProcedureTypesRepo.save(userProcedureTypes);
            return ProcedureSubTypeDTO.builder()
                    .userId(procedureSubTypeCreateDTO.userId())
                    .mainTypeId(mainType.id())
                    .mainType(mainType.type())
                    .subTypeId(uuid)
                    .subType(typeObject.type())
                    .build();
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<ProcedureMainTypeDTO> getAllByUser(String userId) {
        try {
            User user = userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            //TODO
            Optional<UserProcedureTypes> opUserProcedureTypes = userProcedureTypesRepo.findByUserAndIsDeletedFalse(user);
            UserProcedureTypes userProcedureTypes;
            userProcedureTypes = opUserProcedureTypes.orElseGet(() -> userProcedureTypesRepo.insert(UserProcedureTypes.builder().user(user).subTypes(new HashSet<>()).build()));
            System.out.println("userProcedureTypes: " + userProcedureTypes);
            SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(user.getSpeciality()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
            System.out.println("specialityTypes: " + specialityTypes);
            return ClassMapper.INSTANCE.subTypesToProcedureMainTypeDTOs(specialityTypes, userProcedureTypes.getSubTypes());
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public ProcedureMainTypeDTO adminCreateMainType(ProcedureMainTypeCreateDTO procedureMainTypeCreateDTO) {
        try {
            SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(procedureMainTypeCreateDTO.speciality()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "speciality not found"));
            TypeObject typeObject = TypeObject.builder()
                    .id(UUID.randomUUID())
                    .type(procedureMainTypeCreateDTO.mainType())
                    .build();
            if(specialityTypes.getMainTypes() == null){
                specialityTypes.setMainTypes(new HashSet<>());
            }
            specialityTypes.getMainTypes().add(typeObject);
//            specialityTypesRepo.delete(specialityTypes);
            specialityTypesRepo.save(specialityTypes);
            return ClassMapper.INSTANCE.entityToDTO(typeObject, new HashSet<>());

        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

//    public List<ProcedureMainTypeDTO> getAllMainBySpecTypeIn(List<Specialities> specialitiesList) {
//        try {
//            List<SpecialityTypes> specialityTypes = specialityTypesRepo.findBySpecialityInAndIsDeletedFalse(specialitiesList);
//
//        } catch (ResponseStatusException e){
//            throw e;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
//        }
//    }

    //
    public List<ProcedureSpecialityTypeDTO> getAll() {
        try {
            List<SpecialityTypes> specialityTypes = specialityTypesRepo.findAll();
            return ClassMapper.INSTANCE.specialityTypesEntityListToDTOList(specialityTypes);
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public Object addSpecialityTypes(Specialities speciality) {
        try {
            Object obj = specialityTypesRepo.insert(SpecialityTypes.builder().speciality(speciality).mainTypes(new HashSet<>()).build());
            return obj;
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }

    public List<ProcedureMainTypeDTO> getAllDentalByUser(String userId) {
        try {
            User user = userRepo.findByIdAndIsDeletedFalse(userId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "user not found"));
            Optional<UserProcedureTypes> opUserProcedureTypes = userProcedureTypesRepo.findByUserAndIsDeletedFalse(user);
            UserProcedureTypes userProcedureTypes;
            userProcedureTypes = opUserProcedureTypes.orElseGet(() -> userProcedureTypesRepo.insert(UserProcedureTypes.builder().user(user).subTypes(new HashSet<>()).build()));
            System.out.println("userProcedureTypes: " + userProcedureTypes);
            SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(Specialities.DENTAL).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
            specialityTypes.getMainTypes().removeIf(typeObject -> Objects.equals(typeObject.type(), "file") || Objects.equals(typeObject.type(), "toothFile"));
            System.out.println("specialityTypes: " + specialityTypes);
            return ClassMapper.INSTANCE.subTypesToProcedureMainTypeDTOs(specialityTypes, userProcedureTypes.getSubTypes());
        } catch (ResponseStatusException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
        }
    }
}
