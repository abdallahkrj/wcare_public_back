package com.example.wcare.repositories;

import com.example.wcare.data.entities.Clinic;
import com.example.wcare.data.entities.Patient;
import com.example.wcare.data.entities.Procedure;
import com.example.wcare.data.object.Specialities;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProcedureRepo extends MongoRepository<Procedure, String> {
    List<Procedure> findAllByPatientAndClinicAndIsDeletedFalse(ObjectId patient, ObjectId clinic);

    List<Procedure> findAllByPatientAndIsDeletedFalse(ObjectId patient);

    List<Procedure> findAllByPatientAndSpecialityAndIsDeletedFalse(ObjectId patientId, Specialities speciality);

//    Map<Short, List<Procedure>> findAllByPatientAndSpecialityAndIsDeletedFalseGroupByDetailsToothNumber(ObjectId id, Specialities specialities);
}
