package com.example.wcare.repositories;

import com.example.wcare.data.entities.Privilege;
import com.example.wcare.data.object.Privileges;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PrivilegesRepo extends MongoRepository<Privilege, String> {
    List<Privilege> findAllByIdInAndIsDeletedFalse(List<String> ids);

    List<Privilege> findAllByIsDeletedFalse();

    List<Privilege> findByNameInAndIsDeletedFalse(List<Privileges> privileges);
}
