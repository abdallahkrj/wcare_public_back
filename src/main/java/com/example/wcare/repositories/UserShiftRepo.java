package com.example.wcare.repositories;

import com.example.wcare.data.entities.UserShift;
import com.example.wcare.data.iterables.StreamableUserShifts;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserShiftRepo extends MongoRepository<UserShift, String> {
//    List<UserShift> findAllByClinic(ObjectId clinic);
    StreamableUserShifts findAllByClinic(ObjectId clinic);
}
