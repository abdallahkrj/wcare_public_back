package com.example.wcare.repositories;

import com.example.wcare.data.entities.FileModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FileRepo extends MongoRepository<FileModel, String> {
    FileModel findByIdAndIsDeletedFalse(String id);
}
