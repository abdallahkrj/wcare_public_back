package com.example.wcare.repositories;

import com.example.wcare.data.entities.Subscription;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface SubscriptionRepo extends MongoRepository<Subscription, String> {
    Optional<Subscription> findByIdAndIsDeletedIsFalse(String id);
}
