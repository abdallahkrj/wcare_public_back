package com.example.wcare.repositories;

import com.example.wcare.data.entities.Appointment;
import com.example.wcare.data.entities.Clinic;
import com.example.wcare.data.entities.Patient;
import com.example.wcare.data.entities.User;
import com.example.wcare.data.object.AppointmentStatus;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface AppointmentRepo extends MongoRepository<Appointment, String> {
    List<Appointment> findAllByUserAndIsDeletedFalse(User users);
    List<Appointment> findAllByUserAndIsDeletedFalse(ObjectId user);
    List<Appointment> findAllByUserAndStatusInAndIsDeletedFalse(    User users, List<AppointmentStatus> statuses);
    List<Appointment> getAllByUserInAndClinicInAndIsDeletedFalse(List<User> users, List<Clinic> clinic);
    List<Appointment> getAllByUserInAndClinicInAndStatusInAndIsDeletedFalse(List<User> users, List<Clinic> clinic, List<AppointmentStatus> statuses);
    List<Appointment> getAllByClinicAndIsDeletedFalse(Clinic clinic);
    List<Appointment> getAllByClinicAndStatusInAndIsDeletedFalse(Clinic clinic, List<AppointmentStatus> statuses);

    List<Appointment> findAllByPatientAndDateOfReservationBetweenAndIsDeletedFalse(ObjectId patient, LocalDate from, LocalDate to);

    List<Appointment> findAllByStatusInAndPatientAndDateOfReservationBetweenAndIsDeletedFalse(List<AppointmentStatus> statuses, ObjectId patient, LocalDate from, LocalDate to);

    List<Appointment> findAllByUserAndDateOfReservationBetweenAndIsDeletedFalse(User user, LocalDate from, LocalDate to);

    List<Appointment> findAllByStatusInAndUserAndDateOfReservationBetweenAndIsDeletedFalse(List<AppointmentStatus> statuses, User user, LocalDate from, LocalDate to);

    Optional<Appointment> findByIdAndIsDeletedFalse(String id);

    List<Appointment> findAllByPatientAndIsDeletedFalse(ObjectId patient);

    List<Appointment> findAllByStatusInAndPatientAndIsDeletedFalse(List<AppointmentStatus> status, ObjectId patient);
}
