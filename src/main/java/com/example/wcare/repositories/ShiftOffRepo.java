package com.example.wcare.repositories;

import com.example.wcare.data.entities.Clinic;
import com.example.wcare.data.entities.ShiftOff;
import com.example.wcare.data.iterables.StreamableOffShifts;
import com.example.wcare.data.iterables.StreamableUserShifts;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ShiftOffRepo extends MongoRepository<ShiftOff, String> {

    StreamableOffShifts findAllByClinic(ObjectId clinicId);
}
