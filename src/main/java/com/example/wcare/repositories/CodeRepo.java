package com.example.wcare.repositories;

import com.example.wcare.data.entities.Code;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CodeRepo extends MongoRepository<Code, String> {
    Optional<Code> findByCodeAndIsDeletedFalse(String code);
//    List<PasswordResetCode> findByCodeWithCreatedAtLargerThan(String code);
}
