package com.example.wcare.repositories;

import com.example.wcare.data.entities.MedicalHistory;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface MedicalHistoryRepo extends MongoRepository<MedicalHistory, String> {
    Optional<MedicalHistory> findByIdAndIsDeletedFalse(String id);
}
