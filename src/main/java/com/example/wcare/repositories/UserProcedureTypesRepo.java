package com.example.wcare.repositories;

import com.example.wcare.data.entities.User;
import com.example.wcare.data.entities.UserProcedureTypes;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Component
public interface UserProcedureTypesRepo extends MongoRepository<UserProcedureTypes, String> {

//    Optional<ProcedureType> findByUserAndProcedureMainTypeAndIsDeletedIsFalse(User user, ProcedureMainType ProcedureMainType);

    Optional<UserProcedureTypes> findByUserAndIsDeletedFalse(User user);

    Optional<UserProcedureTypes> findByIdAndIsDeletedFalse(String id);

}
