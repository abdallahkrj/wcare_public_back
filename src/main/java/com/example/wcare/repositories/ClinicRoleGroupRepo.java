package com.example.wcare.repositories;

import com.example.wcare.data.entities.Clinic;
import com.example.wcare.data.entities.ClinicRoleGroup;
import com.example.wcare.data.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ClinicRoleGroupRepo extends MongoRepository<ClinicRoleGroup, String> {
    List<ClinicRoleGroup> findAllByClinicInAndUsersAndIsDeletedFalse(List<Clinic> clinics, User user);
    Optional<ClinicRoleGroup> findByClinicAndUsersAndIsDeletedFalse(Clinic clinic, User user);

    Optional<ClinicRoleGroup> findByIdAndIsDeletedFalse(String id);

    List<ClinicRoleGroup> findAllByClinicAndIsDeletedFalse(Clinic clinic);

    List<ClinicRoleGroup> findAllByUsersAndIsDeletedFalse(User user);
}
