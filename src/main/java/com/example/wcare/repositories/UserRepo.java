package com.example.wcare.repositories;

import com.example.wcare.data.entities.Clinic;
import com.example.wcare.data.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends MongoRepository<User, String> {
    Optional<User> findByNumberAndPasswordAndIsDeletedFalse(String number, String password);

    Optional<User> findByEmailAndIsDeletedFalse(String email);

    List<User> findAllByClinics(Clinic clinic);
    List<User> findAllByClinicsAndIsDeletedFalse(Clinic clinic);

    Optional<User> findByNumberAndIsDeletedFalse(String number);

    List<User> findAllByIdAndIsDeletedFalse(List<String> userIds);

    Optional<User> findByIdAndIsDeletedFalse(String userId);

    List<User> findAllByIsDeletedFalse();

    List<User> findByNumberInAndIsDeletedFalse(List<String> strings);
}
