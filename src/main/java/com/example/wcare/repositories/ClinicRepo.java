package com.example.wcare.repositories;

import com.example.wcare.data.entities.Clinic;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ClinicRepo extends MongoRepository<Clinic, String> {
    List<Clinic> findAllByIdAndIsDeletedFalse(List<String> clinicIds);

    List<Clinic> findAllByIsDeletedFalse();

    Optional<Clinic> findByIdAndIsDeletedFalse(String id);

    Optional<Clinic> findByNameAndIsDeletedFalse(String name);
}
