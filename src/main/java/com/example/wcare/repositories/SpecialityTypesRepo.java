package com.example.wcare.repositories;

import com.example.wcare.data.entities.SpecialityTypes;
import com.example.wcare.data.object.Specialities;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpecialityTypesRepo extends MongoRepository<SpecialityTypes, String> {
    Optional<SpecialityTypes> findByIdAndIsDeletedFalse(String id);

    Optional<SpecialityTypes> findBySpecialityAndIsDeletedFalse(Specialities speciality);
}
