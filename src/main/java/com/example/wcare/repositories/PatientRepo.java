package com.example.wcare.repositories;

import com.example.wcare.data.entities.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface PatientRepo extends MongoRepository<Patient, String> {
    //    Optional<List<MedicalHistory>> updateMedicalHistories(List<MedicalHistoryDTO> medicalHistoryDTOS);
    Optional<Patient> findByNumberAndIsDeletedFalse(String number);

    Optional<Patient> findByIdAndIsDeletedFalse(String patientId);

    List<Patient> findAllByIsDeletedFalse();

    Optional<Patient> findByNumberAndPasswordAndIsDeletedFalse(String number, String password);
}
