package com.example.wcare.repositories;

import com.example.wcare.data.entities.SubscriptionPlan;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface PlanRepo extends MongoRepository<SubscriptionPlan, String> {

    List<SubscriptionPlan> findAllByIsDeletedFalse();

    Optional<SubscriptionPlan> findByIdAndIsDeletedFalse(String id);
    Optional<SubscriptionPlan> findByNameAndIsDeletedFalse(String name);
}
