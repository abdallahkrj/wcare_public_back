package com.example.wcare.data.entities;


import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Document(collection = "clinic")
@ToString
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Clinic extends DocumentMetaData {
    public Clinic(String id,
                  String name,
                  String number,
                  String address,
                  Map<String, String> userColors,
                  Map<String, Object> settings,
                  String specialties,
//                  ClinicUserShifts clinicUserShifts,
//                  ShiftOff shiftOff,
                  List<FileModel> files,
                  List<User> users,
                  List<Subscription> subscriptions,
                  SubscriptionPlan plan,
                  List<ClinicRoleGroup> clinicUserGroupPrivilegesList
                  ) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.address = address;
        this.userColors = userColors;
        this.settings = settings;
        this.specialties = specialties;
//        this.clinicUserShifts = clinicUserShifts;
//        this.shiftOff = shiftOff;
        this.files = files;
        this.users = users;
        this.subscriptions = subscriptions;
        if(plan == null && this.subscriptions != null){
            this.plan = plan();
        }else{
            this.plan = plan;
        }
        this.clinicUserGroupPrivilegesList = clinicUserGroupPrivilegesList;
    }

    @Id
    private String id;
    private String name;
    private String number; //removed if center not null
    private String address; //removed if center not null
    private Map<String, String> userColors; //userId: color
    private Map<String, Object> settings; //for frontend
    private String specialties;
//    private ClinicUserShifts clinicUserShifts;
//    private ShiftOff shiftOff;
    @DocumentReference
    private List<FileModel> files;
    @DocumentReference(lazy = true, lookup = "{ 'clinics' : ?#{#self._id} }")
    @ReadOnlyProperty
    private List<User> users;
    @DocumentReference(lazy = false, lookup = "{ 'clinic' : ?#{#self._id} }")
    @ReadOnlyProperty
    private List<Subscription> subscriptions;
    @ReadOnlyProperty
    private SubscriptionPlan plan;
    @DocumentReference(lazy = true, lookup = "{ 'clinic' : ?#{#self._id} }")
    @ReadOnlyProperty
    private List<ClinicRoleGroup> clinicUserGroupPrivilegesList;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Clinic clinic = (Clinic) o;
        return Objects.equals(getId(), clinic.getId());
    }

    public SubscriptionPlan plan() {
        return subscriptions.stream().filter(subscription -> subscription.date.plusDays((long) subscription.plan.getDuration()).isAfter(LocalDate.now())).findFirst().get().getPlan();
    }
}
