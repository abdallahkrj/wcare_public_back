package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Document(collection = "subscriptionPlan")
@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubscriptionPlan extends DocumentMetaData {
    @Id
    private String id;
    private String name;
    private double duration;
    private int maxUserCount;
    @DocumentReference
    private List<Privilege> privileges;
    @DocumentReference(lazy = true, lookup = "{ 'plan' : ?#{#self._id} }")
    @ReadOnlyProperty
    private List<Clinic> clinics;
}
