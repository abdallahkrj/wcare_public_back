package com.example.wcare.data.entities;
import com.example.wcare.data.object.DocumentMetaData;
import com.example.wcare.data.object.MedicalHistoryType;
import com.mongodb.lang.Nullable;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDateTime;

@Document(collection = "medical_histories")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class MedicalHistory extends DocumentMetaData{
    @Id
    private String id;
    private MedicalHistoryType type;
    private String description;
    private LocalDateTime dateTime;
    private Boolean status;
    @Nullable
    private FileModel attachment;
    @DocumentReference(lazy = true, lookup = "{ 'medicalHistories' : ?#{#self._id} }")
    @ReadOnlyProperty
    private Patient patient;
}
