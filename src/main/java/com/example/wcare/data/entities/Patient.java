package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Document(collection = "patient")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Patient extends DocumentMetaData {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String address;
    @Indexed(unique = true)
    private String number;
    @Indexed(unique = true)
    private String email;
    private String password;
    @Indexed(unique = true)
    protected String token;
    private boolean isActivated;
    @DocumentReference(lazy = true, lookup = "{ 'patient' : ?#{#self._id} }")
    @ReadOnlyProperty
    private List<Procedure> procedures;
    @DocumentReference
    private List<MedicalHistory> medicalHistories;

    @Override
    public boolean equals(Object o) {
        System.out.println("equals");
        System.out.println(o == null? "o": o.getClass());
        System.out.println(getClass());
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        System.out.println(getId());
        System.out.println(patient.getId());
        return Objects.equals(getId(), patient.getId());
    }
}