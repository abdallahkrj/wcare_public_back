package com.example.wcare.data.entities;

import com.example.wcare.data.object.AppointmentStatus;
import com.example.wcare.data.object.DocumentMetaData;
import com.example.wcare.data.object.Summary;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "appointment")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Appointment extends DocumentMetaData {
    @Id
    private String id;
    private String title;
    private List<String> notes;
    private LocalDateTime dateOfReservation;
    private Double duration;
    private Summary summary;
    private String type;
    private AppointmentStatus status;
    @DocumentReference(lazy = true)
    private Clinic clinic;
    @DocumentReference(lazy = true)
    private Patient patient;
    @DocumentReference(lazy = true)
    private User user;
}
