package com.example.wcare.data.entities;


import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@Document(collection = "privileges")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Privilege extends DocumentMetaData {
    @Id
    private String id;
    @Indexed(unique = true)
    private String name;
    private String description;
    //todo: need to add privilege named doctor to know who is the doctors in clinic


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Privilege that = (Privilege) o;
        return Objects.equals(id, that.id);
    }
}
