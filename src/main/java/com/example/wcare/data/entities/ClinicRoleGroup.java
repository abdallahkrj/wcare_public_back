package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Document(collection = "clinicRoleGroup")
@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClinicRoleGroup extends DocumentMetaData {
    @Id
    private String id;
    private String name;
    @DocumentReference
    private List<Privilege> privileges;
    @DocumentReference
    private Clinic clinic;
    @DocumentReference
    private List<User> users;
}
