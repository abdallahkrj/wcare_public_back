package com.example.wcare.data.entities;

import com.example.wcare.data.object.ProcedureDetails;
import com.example.wcare.data.object.DocumentMetaData;
import com.example.wcare.data.object.Specialities;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDateTime;
import java.util.UUID;

@Document(collection = "procedure")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Procedure extends DocumentMetaData {
    @Id
    private String id;
    private UUID subTypeId;
    private UUID mainTypeId;
    private Specialities speciality;
    @DocumentReference
    private User user;
    @DocumentReference
    private Clinic clinic;
    @DocumentReference
    private Patient patient;
    private LocalDateTime procedureDate;
    private ProcedureDetails details;
}
