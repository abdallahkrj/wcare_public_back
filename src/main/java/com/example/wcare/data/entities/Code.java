package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "passwordResetCode")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Code extends DocumentMetaData {
    @Id
    private String id;
    @Indexed(unique = true)
    private String code;
    private String email;
}
