package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import com.example.wcare.data.object.Payment;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDateTime;
import java.util.List;

@Document(collection = "charge")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Charge extends DocumentMetaData {
    @Id
    private String id;
    private long fullPrice;
    private LocalDateTime nextPaymentDate;
    private List<Payment> payments;
    @DocumentReference
    private Patient patient;
    @DocumentReference
    private Clinic clinic;
}
