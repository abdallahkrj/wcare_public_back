package com.example.wcare.data.entities;

import com.example.wcare.data.object.DayShift;
import jakarta.annotation.Nullable;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.DayOfWeek;
import java.util.Map;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Document(collection = "userShifts")
public class UserShift {
    @Id
    String id;
    @DocumentReference(lazy = true)
    User user;
    @DocumentReference(lazy = true)
    Clinic clinic;

    Map<DayOfWeek, DayShift> daysShifts;

//    @Nullable
//    DayShift sat;
//    @Nullable
//    DayShift sun;
//    @Nullable
//    DayShift mon;
//    @Nullable
//    DayShift tue;
//    @Nullable
//    DayShift wed;
//    @Nullable
//    DayShift thu;
//    @Nullable
//    DayShift fri;
}
