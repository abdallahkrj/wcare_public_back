package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import com.example.wcare.data.object.Specialities;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;
import java.util.Objects;

@Document(collection = "user")
@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User extends DocumentMetaData {
    @Id
    protected String id;
    private String firstName;
    private String lastName;
    @Indexed(unique = true)
    private String email;
    private Specialities speciality;
    @Indexed(unique = true)
    private String number;
    private String password;
    private String token;
    @DocumentReference
    private List<Clinic> clinics;
    @DocumentReference(lazy = true, lookup = "{ 'users' : ?#{#self._id} }")
    @ReadOnlyProperty
    private List<ClinicRoleGroup> clinicPrivileges;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }
}