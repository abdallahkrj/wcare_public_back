package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "file")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class FileModel extends DocumentMetaData {
    @Id
    private String id;
    private String type;
    @Lazy
    private byte[] file;
}
