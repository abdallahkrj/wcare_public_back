package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import com.example.wcare.data.object.SubTypeObject;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.Set;

@Document(collection = "user_procedure_types")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserProcedureTypes extends DocumentMetaData { //by and for user
    @Id
    private String id;
    @DocumentReference
    @Indexed(unique = true)
    private User user;
    private Set<SubTypeObject> subTypes;
}