package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDate;

@Document(collection = "subscription")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Subscription extends DocumentMetaData {
    @Id
    String id;
    @DocumentReference
    SubscriptionPlan plan;
    @DocumentReference
    Clinic clinic;
    LocalDate date;
}
