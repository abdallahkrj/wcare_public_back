package com.example.wcare.data.entities;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.time.LocalDateTime;

@Document(collection = "shiftOff")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ShiftOff {
    @Id
    String id;
    @DocumentReference
    Clinic clinic;
    @DocumentReference
    User user;
    LocalDateTime from;
    LocalDateTime to;
}
