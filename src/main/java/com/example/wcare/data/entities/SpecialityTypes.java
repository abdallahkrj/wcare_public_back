package com.example.wcare.data.entities;

import com.example.wcare.data.object.DocumentMetaData;
import com.example.wcare.data.object.Specialities;
import com.example.wcare.data.object.TypeObject;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;
import java.util.UUID;

@Document(collection = "speciality_type")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class SpecialityTypes extends DocumentMetaData { //by ADMIN DASHBOARD
    @Id
    private String id;
    @Indexed(unique = true)
    private Specialities speciality;
    private Set<TypeObject> mainTypes;

    public UUID getTypeUUID(String name){
        return mainTypes.stream().filter(typeObject -> typeObject.type().equals(name)).findFirst().orElseThrow().id();
    }
    public TypeObject getType(UUID id){
        return mainTypes.stream().filter(typeObject -> typeObject.id().equals(id)).findFirst().orElseThrow();
    }
}