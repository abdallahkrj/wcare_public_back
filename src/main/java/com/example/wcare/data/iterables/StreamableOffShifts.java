package com.example.wcare.data.iterables;

import com.example.wcare.data.entities.ShiftOff;
import com.example.wcare.data.entities.UserShift;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.util.Streamable;

import java.time.LocalDateTime;
import java.util.Iterator;

@RequiredArgsConstructor(staticName = "of")
public class StreamableOffShifts implements Streamable<ShiftOff> {
    private final Streamable<ShiftOff> streamable;

    public boolean TimeOff(LocalDateTime time){
        return stream().anyMatch(shiftOff -> time.isAfter(shiftOff.getFrom()) && time.isBefore(shiftOff.getTo()) );
    }

    @NotNull
    @Override
    public Iterator<ShiftOff> iterator() {
        return streamable.iterator();
    }
}
