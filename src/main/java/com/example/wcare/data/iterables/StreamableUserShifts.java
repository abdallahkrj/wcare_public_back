package com.example.wcare.data.iterables;

import com.example.wcare.data.entities.UserShift;
import com.example.wcare.data.object.DayShift;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.util.Streamable;

import java.time.LocalDateTime;
import java.util.Iterator;

@RequiredArgsConstructor(staticName = "of")
public class StreamableUserShifts implements Streamable<UserShift> {
    private final Streamable<UserShift> streamable;

    public boolean TimeInShift(LocalDateTime time) {
        return stream().anyMatch(userShift -> {
            DayShift dayShift = userShift.getDaysShifts().get(time.getDayOfWeek());
            return dayShift.getStartTime().isBefore(time.toLocalTime()) && dayShift.getEndTime().isAfter(time.toLocalTime());
        });
    }

    @NotNull
    @Override
    public Iterator<UserShift> iterator() {
        return streamable.iterator();
    }
}
