package com.example.wcare.data.models;

import com.example.wcare.data.object.AppointmentStatus;
import com.example.wcare.data.object.Summary;

import java.time.LocalDateTime;
import java.util.List;

public record AppointmentModel(
    String id,
    String title,
    LocalDateTime dateOfReservation,
    List<String> notes,
    Summary summary,
    String type,
    double duration,
    AppointmentStatus status
){}


