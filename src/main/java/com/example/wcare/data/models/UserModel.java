package com.example.wcare.data.models;


public record UserModel(
         String id,
         String firstName,
         String lastName,
         String email,
         String number,
         String password,
         String token
) {
}
