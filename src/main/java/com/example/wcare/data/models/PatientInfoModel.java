package com.example.wcare.data.models;

import java.time.LocalDate;
import java.util.Date;

public record PatientInfoModel(
         String id,
         String firstName,
         String lastName,
         LocalDate birthDate,
         String address,
         String number,
         String email
) {
}
