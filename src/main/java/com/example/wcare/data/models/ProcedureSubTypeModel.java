package com.example.wcare.data.models;

import lombok.Builder;

import java.util.UUID;

@Builder
public record ProcedureSubTypeModel(
        UUID subTypeId,
        String subType
) {
}