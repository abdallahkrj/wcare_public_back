package com.example.wcare.data.models;

import java.util.Map;

public record CenterModel(
         String id,
         String name,
         String number,
         String address,
         String themeColor,
         Map<String, String>settings
) {
}
