package com.example.wcare.data.models;

import com.example.wcare.data.entities.SpecialityTypes;

public record ProcedureTypeInfoModel(
        String id,
        SpecialityTypes specialityTypes,
        String subType
) {
}