package com.example.wcare.data.models;


public record UserInfoModel(
         String id,
         String firstName,
         String lastName,
         String email,
         String number
) {
}
