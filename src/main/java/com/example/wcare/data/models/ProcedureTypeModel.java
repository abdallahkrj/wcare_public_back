package com.example.wcare.data.models;

import com.example.wcare.data.entities.SpecialityTypes;

public record ProcedureTypeModel(
        String id,
        UserInfoModel user,
        SpecialityTypes specialityTypes,
        String subType
) {
}