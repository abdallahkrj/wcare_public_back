package com.example.wcare.data.models;

import lombok.Builder;

import java.util.Map;

@Builder
public record ClinicModel(
        String id,
        String name,
        String number,
        String address,
        String specialties,
        Map<String, String> userColors,
        Map<String, Object> settings,
        SubscriptionPlanModel plan
) {
}
