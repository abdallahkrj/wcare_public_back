package com.example.wcare.data.models;

import java.util.List;

public record ClinicUserPrivilegesGroupModel(
        String id,
        String name,
        List<PrivilegesModel> privileges,
        List<UserInfoModel> users,
        ClinicInfoModel clinic
){
}
