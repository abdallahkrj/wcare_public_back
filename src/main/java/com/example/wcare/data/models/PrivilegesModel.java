package com.example.wcare.data.models;

public record PrivilegesModel(
        String id,
        String name,
        String description
) {}
