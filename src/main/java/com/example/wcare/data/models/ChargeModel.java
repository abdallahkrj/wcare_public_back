package com.example.wcare.data.models;

import com.example.wcare.data.object.Payment;

import java.time.LocalDateTime;
import java.util.List;

public record ChargeModel(
         String id,
         long fullPrice,
         LocalDateTime nextPaymentDate,
         List<Payment>payments
) {
}
