package com.example.wcare.data.models;

import com.example.wcare.data.object.ProcedureDetails;

import java.time.LocalDateTime;

public record ProcedureModel (
         String id,
         String type,
         LocalDateTime procedureDate,
         ProcedureDetails details
){
}
