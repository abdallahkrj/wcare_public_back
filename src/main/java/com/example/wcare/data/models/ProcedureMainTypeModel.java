package com.example.wcare.data.models;

import lombok.Builder;

import java.util.List;
import java.util.UUID;

@Builder
public record ProcedureMainTypeModel(
        UUID mainTypeId,
        String mainType,
        List<ProcedureSubTypeModel> procedureSubTypeModels
) {
}