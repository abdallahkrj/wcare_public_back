package com.example.wcare.data.models;

import com.example.wcare.dto.procedures.ProcedureMetaDataDTO;

import java.util.List;

public record SummaryModel (
        String description,

        List<ProcedureMetaDataDTO> procedures
){}
