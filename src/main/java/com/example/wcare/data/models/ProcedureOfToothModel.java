package com.example.wcare.data.models;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public class ProcedureOfToothModel {
        private String mainType;
        private String subType;
        private UserInfoModel user;
        private LocalDateTime procedureDate;

}
