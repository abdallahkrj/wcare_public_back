package com.example.wcare.data.models;

import java.util.Map;

public record ClinicInfoModel(
        String id,
        String name,
        String number,
        String address,
        Map<String, Object> settings,
        String specialties
) {}
