package com.example.wcare.data.models;

public record SubscriptionPlanModel(
         String id,
         String name,
         double duration,
         int maxUserCount
) {
}
