package com.example.wcare.data.models;

public record FileModel(
         String id,
         String type
){
}
