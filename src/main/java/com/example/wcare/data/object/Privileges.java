package com.example.wcare.data.object;

public enum Privileges {
    //users
    addPatient,
    editClinicInfo,
    editClinicsUsers,
    editClinicsRoleGroups,
    editUsersColors,
    editUsersShifts,
    editUsersDaysOff,
    editSubProceduresType,
    editSubProceduresTypeToOthers,
    //appointments
    haveAppointments,
    addAppointments,
    addAppointmentsToOther,
    deleteAppointment,
    deleteAppointmentsOfOthers,
    viewAppointments,
    //medical history
    editPatientsProcedures,
    editPatientsMedicalHistory,
    editPatientsHistoryFiles,
    haveShift
}
