package com.example.wcare.data.object;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Medical History Type Enum", enumAsRef = true)
public enum MedicalHistoryType {
    ILLNESSES_HISTORY,
    MEDICINE_HISTORY,
    GENETIC_HISTORY,
    SURGERY_HISTORY,
    ALLERGIES_HISTORY,
    PATIENT_BEHAVIOR,
    NOTES,
    ATTACHMENTS,
}
