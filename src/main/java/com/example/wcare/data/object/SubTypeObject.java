package com.example.wcare.data.object;

import lombok.Builder;

import java.util.Set;
import java.util.UUID;

@Builder
public record SubTypeObject(UUID mainId, Set<TypeObject> subTypes) {
}