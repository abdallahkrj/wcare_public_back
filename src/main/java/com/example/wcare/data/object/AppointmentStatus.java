package com.example.wcare.data.object;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.models.annotations.OpenAPI30;

@Schema(description = "Appointment Status Enum")
public enum AppointmentStatus {
    DONE,
    PENDING,
    IGNORED,
    REJECTED
}
