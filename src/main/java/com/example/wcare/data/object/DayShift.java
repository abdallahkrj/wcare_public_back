package com.example.wcare.data.object;

import lombok.*;

import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
//todo need validation
public class DayShift {
    LocalTime startTime;
    LocalTime endTime;
}
