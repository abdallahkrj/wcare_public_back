package com.example.wcare.data.object;

import com.example.wcare.data.models.FileModel;
import lombok.*;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DentalFile extends ProcedureDetails {
//    @DocumentReference
    private FileModel file;
}