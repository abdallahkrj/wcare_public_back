package com.example.wcare.data.object;

import lombok.Builder;

import java.util.UUID;

@Builder
public record TypeObject(UUID id, String type) {
}
