package com.example.wcare.data.object;

import com.example.wcare.data.entities.Procedure;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Summary {
    private String description;
    @DocumentReference
    private List<Procedure> procedures;
}
