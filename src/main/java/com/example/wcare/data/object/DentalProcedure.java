package com.example.wcare.data.object;

import lombok.*;

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DentalProcedure extends ProcedureDetails {
    private short toothNumber;
    private String description;
}