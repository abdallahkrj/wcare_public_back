package com.example.wcare.util;

import com.example.wcare.data.entities.*;
import com.example.wcare.data.entities.FileModel;
import com.example.wcare.data.models.*;
import com.example.wcare.data.entities.UserShift;
import com.example.wcare.dto.appointment.AppointmentCreateDTO;
import com.example.wcare.dto.appointment.AppointmentDTO;
import com.example.wcare.dto.clinic.*;
import com.example.wcare.dto.clinicUserGroupPrivileges.ClinicUserGroupPrivilegesDTO;
import com.example.wcare.dto.medicalHistory.MedicalHistoryDTO;
import com.example.wcare.dto.patient.PatientDTO;
import com.example.wcare.dto.patient.PatientInfoDTO;
import com.example.wcare.dto.patient.PatientSignupDTO;
import com.example.wcare.dto.privileges.PrivilegesCreateDTO;
import com.example.wcare.dto.privileges.PrivilegesDTO;
import com.example.wcare.dto.procedures.*;
import com.example.wcare.dto.subscriptionPlan.SubscriptionPlanCreateDTO;
import com.example.wcare.dto.subscriptionPlan.SubscriptionPlanDTO;
import com.example.wcare.dto.user.UserDTO;
import com.example.wcare.dto.user.UserDataDTO;
import com.example.wcare.data.object.SubTypeObject;
import com.example.wcare.data.object.TypeObject;
import com.example.wcare.repositories.SpecialityTypesRepo;
import com.example.wcare.repositories.UserProcedureTypesRepo;
import jakarta.validation.constraints.Null;
import org.apache.commons.lang3.ObjectUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Mapper
@Component
public abstract class ClassMapper {
    public static ClassMapper INSTANCE = Mappers.getMapper(ClassMapper.class);

    private static UserProcedureTypesRepo userProcedureTypesRepo;
    private static SpecialityTypesRepo specialityTypesRepo;

    @Autowired
    public final void setRepos(UserProcedureTypesRepo userProcedureTypesRepo, SpecialityTypesRepo specialityTypesRepo) {
        ClassMapper.userProcedureTypesRepo = userProcedureTypesRepo;
        ClassMapper.specialityTypesRepo = specialityTypesRepo;
        System.out.println("Autowired:Repo" + ClassMapper.userProcedureTypesRepo);
        System.out.println("Autowired:Repo" + ClassMapper.specialityTypesRepo);
    }

    // Entity to DTO
    public abstract PatientDTO entityToDTO(Patient patient);

    public abstract UserDTO entityToDTO(User user);

    public abstract AppointmentDTO entityToDTO(Appointment appointment);

    public abstract MedicalHistoryDTO entityToDTO(MedicalHistory medicalHistory);

    // DTO to Entity
    public abstract Patient DTOtoEntity(PatientDTO patientDTO);

    public abstract Patient DTOtoEntity(PatientSignupDTO patientSignupDTO);

    public abstract User DTOtoEntity(UserDTO userDTO);

    //    public abstract User DTOtoEntity(UserSignupDTO userSignupDTO);
    public abstract Appointment DTOtoEntity(AppointmentCreateDTO appointmentCreateDTO);

    public abstract Appointment DTOtoEntity(AppointmentDTO appointmentDTO);

    public abstract MedicalHistory DTOtoEntity(MedicalHistoryDTO medicalHistoryDTO);

    // Entity List to DTO List
    public abstract List<UserDTO> userEntityListToDTOList(List<User> users);

    public abstract List<AppointmentDTO> appointmentEntityListToDTOList(List<Appointment> appointments);

    public abstract List<PatientDTO> patientEntityListToDTOList(List<Patient> patients);

    public List<PatientInfoDTO> patientEntityMapToInfoDTOList(Map<Clinic, Patient> patients) {
        if (patients == null) {
            return null;
        }
        List<PatientInfoDTO> list = new ArrayList<>(patients.size());
        patients.forEach((clinic, patient) -> {
            list.add(entityToInfoDTO(clinic.getId(), clinic.getName(), patient));
        });
        return list;
    }

    public PatientInfoDTO entityToInfoDTO(String clinicId, String clinicName, Patient patient) {
        return PatientInfoDTO.builder()
                .id(patient.getId())
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .email(patient.getEmail())
                .number(patient.getNumber())
                .address(patient.getAddress())
                .birthDate(patient.getBirthDate())
                .clinicId(clinicId)
                .clinicName(clinicName)
                .build();
    }

    public abstract List<MedicalHistoryDTO> medicalHistoryEntityListToDTOList(List<MedicalHistory> medicalHistories);

    public abstract List<ClinicDTO> clinicEntityListToDTOList(List<Clinic> clinics);

    public abstract List<SubscriptionPlanDTO> planEntityListToDTOList(List<SubscriptionPlan> planList);

    public abstract List<PrivilegesDTO> privilegesEntityListToDTOList(List<Privilege> privileges);

    @Mapping(target = "users", source = "clients")
    public abstract Clinic DTOtoEntity(ClinicDTO clinicDTO);

    @Mapping(source = "users", target = "clients")
    public abstract ClinicDTO entityToDTO(Clinic clinic);

    public abstract SubscriptionPlan DTOtoEntity(SubscriptionPlanDTO subscriptionPlanDTO);

    public abstract SubscriptionPlanDTO entityToDTO(SubscriptionPlan subscriptionPlan);

    public abstract SubscriptionPlan DTOtoEntity(SubscriptionPlanCreateDTO planDTO);

    public abstract Privilege DTOtoEntity(PrivilegesCreateDTO privilegesCreateDTO);

    public abstract PrivilegesDTO entityToDTO(Privilege privilege);

    public abstract ClinicUserGroupPrivilegesDTO entityToDTO(ClinicRoleGroup clinicUserGroupPrivileges);

    public abstract List<ClinicUserGroupPrivilegesDTO> clinicUserGroupPrivilegesEntityListToDTOs(List<ClinicRoleGroup> clinicUserGroupPrivilegesList);

    public abstract Procedure DTOtoEntity(DentalProceduresCreateDTO dentalProceduresCreateDTO);

//    public ProcedureMetaDataDTO entityToDTO(Procedure procedure, UserProcedureTypes procedureTypes, SpecialityTypes specialityTypes) {
//        String mainType = specialityTypes.getMainTypes().stream().filter(typeObject -> typeObject.id().equals(procedure.getMainTypeId())).findAny().orElseThrow().type();
//        String subType = null;
//        if(procedure.getSubTypeId() != null) {
//            for (SubTypeObject subTypeList : procedureTypes.getSubTypes()) {
//                if (subTypeList.mainId().equals(procedure.getMainTypeId())) {
//                    for (TypeObject sub : subTypeList.subTypes()) {
//                        if (sub.id().equals(procedure.getSubTypeId())) {
//                            subType = sub.type();
//                        }
//                    }
//                }
//            }
//        }
//        return ProcedureMetaDataDTO.builder()
//                .id(procedure.getId())
//                .details(procedure.getDetails())
//                .procedureDate(procedure.getProcedureDate())
//                .mainType(mainType)
//                .subType(subType)
//                .userId(procedure.getUser().getId())
//                .clinicId(procedure.getClinic().getId())
//                .patientId(procedure.getPatient().getId())
//                .build();
//    }

    public ProcedureMetaDataDTO entityToDTO(Procedure procedure) {
        User user = procedure.getUser();
        UserProcedureTypes procedureTypes = userProcedureTypesRepo.findByUserAndIsDeletedFalse(user).orElseGet(() -> userProcedureTypesRepo.insert(UserProcedureTypes.builder()
                .user(user).subTypes(Set.of()).build()));
        SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(user.getSpeciality()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
        String mainType = specialityTypes.getMainTypes().stream().filter(typeObject -> typeObject.id().equals(procedure.getMainTypeId())).findAny().orElseThrow().type();
        String subType = null;
        if (procedure.getSubTypeId() != null) {
            for (SubTypeObject subTypeList : procedureTypes.getSubTypes()) {
                if (subTypeList.mainId().equals(procedure.getMainTypeId())) {
                    for (TypeObject sub : subTypeList.subTypes()) {
                        if (sub.id().equals(procedure.getSubTypeId())) {
                            subType = sub.type();
                        }
                    }
                }
            }
        }
        return ProcedureMetaDataDTO.builder()
                .id(procedure.getId())
                .details(procedure.getDetails())
                .procedureDate(procedure.getProcedureDate())
                .mainType(mainType)
                .subType(subType)
                .userId(procedure.getUser().getId())
                .clinicId(procedure.getClinic().getId())
                .patientId(procedure.getPatient().getId())
                .build();
    }

    public List<ProcedureMetaDataDTO> entityToDTOList(List<Procedure> procedures){
        return procedures.stream().map(procedure -> {
            return ClassMapper.INSTANCE.entityToDTO(procedure);
        }).toList();
    }
    public List<ProcedureDataDTO> entityToDTO2List(List<Procedure> procedures){
        return procedures.stream().map(procedure -> {
            return ClassMapper.INSTANCE.entityToDTO2(procedure);
        }).toList();
    }
    public ProcedureDataDTO entityToDTO2(Procedure procedure) {
        User user = procedure.getUser();
        Optional<UserProcedureTypes> OpProcedureTypes = userProcedureTypesRepo.findByUserAndIsDeletedFalse(user);
        UserProcedureTypes procedureTypes;
        procedureTypes = OpProcedureTypes.orElseGet(() -> userProcedureTypesRepo.insert(UserProcedureTypes.builder()
                .user(user).subTypes(Set.of()).build()));
        SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(procedure.getSpeciality()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
        String mainType = specialityTypes.getType(procedure.getMainTypeId()).type();
        String subType = null;
        if (procedure.getSubTypeId() != null) {
            for (SubTypeObject subTypeList : procedureTypes.getSubTypes()) {
                if (subTypeList.mainId().equals(procedure.getMainTypeId())) {
                    for (TypeObject sub : subTypeList.subTypes()) {
                        if (sub.id().equals(procedure.getSubTypeId())) {
                            subType = sub.type();
                        }
                    }
                }
            }
        }
        return ProcedureDataDTO.builder()
                .id(procedure.getId())
                .details(procedure.getDetails())
                .procedureDate(procedure.getProcedureDate())
                .mainType(mainType)
                .subType(subType)
                .user(ClassMapper.INSTANCE.entityToModel(procedure.getUser()))
                .clinic(ClassMapper.INSTANCE.entityToInfoModel(procedure.getClinic()))
                .patientId(procedure.getPatient().getId())
                .build();
    }


    public ProcedureMainTypeDTO entityToDTO(TypeObject mainType, Set<TypeObject> typeObjects) {
        List<ProcedureSubTypeModel> procedureSubTypeModels = typeObjects.stream().map(typeObject -> {
            return ProcedureSubTypeModel.builder()
                    .subTypeId(typeObject.id())
                    .subType(typeObject.type())
                    .build();
        }).toList();
        return ProcedureMainTypeDTO.builder()
                .id(mainType.id())
                .type(mainType.type())
                .procedureSubTypeModels(procedureSubTypeModels)
                .build();
    }

    public List<ProcedureMainTypeDTO> subTypesToProcedureMainTypeDTOs(SpecialityTypes specialityTypes, Set<SubTypeObject> subTypeObjects) {
//        return subTypeObjects.stream().map(subTypeObject -> {
//            TypeObject mainType = specialityTypes.getMainTypes().stream().filter(typeObject -> typeObject.id().equals(subTypeObject.mainId())).findAny().orElseThrow(() -> new ResponseStatusException(HttpStatus.CONFLICT, "user has sub type to a non-existing main type"));
//            return ClassMapper.INSTANCE.entityToDTO(mainType, subTypeObject.subTypes());
//        }).toList();
        return specialityTypes.getMainTypes().stream().map(typeObject -> {
            Optional<SubTypeObject> subTypeObj = subTypeObjects.stream().filter(subTypeObject -> subTypeObject.mainId() == typeObject.id()).findAny();
            Set<TypeObject> subTypes;
            if (subTypeObj.isPresent()) {
                subTypes = subTypeObj.get().subTypes();
            } else {
                subTypes = new HashSet<TypeObject>();
            }
            return ClassMapper.INSTANCE.entityToDTO(typeObject, subTypes);
        }).toList();
    }

    public abstract List<ProcedureSpecialityTypeDTO> specialityTypesEntityListToDTOList(List<SpecialityTypes> specialityTypes);

    public abstract List<FileModel> fileModelListToEntityList(List<com.example.wcare.data.models.FileModel> files);

    public List<UserDataDTO> userEntityListToDataDTOList(List<User> users, String clinicId) {
        List<UserDataDTO> list = new ArrayList<UserDataDTO>(users.size());
        for (User user : users) {
            list.add(entityToDataDTO(user, clinicId));
        }
        return list;
    }

    public UserDataDTO entityToDataDTO(User user, String clinicId) {
        final ClinicRoleGroup group = user.getClinicPrivileges().stream().filter(clinicUserGroupPrivileges -> clinicUserGroupPrivileges.getClinic().getId().equals(clinicId)).findAny().orElseThrow();
        return UserDataDTO.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .number(user.getNumber())
                .email(user.getEmail())
                .group(entityToModel(group))
                .build();
    }

    protected abstract ClinicUserPrivilegesGroupModel entityToModel(ClinicRoleGroup group);

    protected ClinicModel entityToModel(Clinic clinic){
        return ClinicModel.builder()
                .name(clinic.getName())
                .plan(entityToModel(clinic.plan()))
                .userColors(clinic.getUserColors())
                .settings(clinic.getSettings())
                .address(clinic.getAddress())
                .number(clinic.getNumber())
                .id(clinic.getId())
                .specialties(clinic.getSpecialties())
                .build();
    }
    protected abstract ClinicInfoModel entityToInfoModel(Clinic clinic);

    protected abstract SubscriptionPlanModel entityToModel(SubscriptionPlan plan);
    protected abstract UserInfoModel entityToModel(User user);

    public abstract List<ShiftOffDTO> shiftOffToDTO(List<ShiftOff> source);
//    @Mapping(source = "user.id", target = "userId")
//    public abstract ShiftOffDTO entityToDTO(ShiftOff source);

    public abstract List<UserShiftDTO> userShiftToDTO(List<UserShift> userShifts);


    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "clinic.id", target = "clinicId")
    public abstract UserShiftDTO entityToDTO(UserShift insert);

    public abstract UserShift dtoToEntity(UserShiftDTO userShiftDTO);
//    public abstract UserShift createEntity(UserShiftCreateDTO userShiftDTO);

    @Mapping(source = "clinic.id", target = "clinicId")
    @Mapping(source = "user.id", target = "userId")
    public abstract ShiftOffDTO entityToDTO(ShiftOff insert);
    public abstract ShiftOff dtoToEntity(ShiftOffDTO shiftOffDTO);

    public User idToUser(String id){
        return User.builder().id(id).build();
    }
    public Clinic idToClinic(String id){
        return Clinic.builder().id(id).build();
    }

    public ClinicUserDTO toDTO(User user, String clinicId, UserShift shift, List<ShiftOff> offList) {
        return ClinicUserDTO.builder()
                .user(ClassMapper.INSTANCE.entityToDataDTO(user, clinicId))
                .shift(ClassMapper.INSTANCE.entityToDTO(shift))
                .shiftOff(ClassMapper.INSTANCE.shiftOffToDTO(offList))
                .build();
    }
    // DTO List to Entity List
}
