package com.example.wcare.util;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.mail.MailMessage;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Slf4j
@Service
public class MailSender {
    private final Environment env;

   @Autowired
   private  JavaMailSender javaMailSender;

   @Autowired
    public MailSender(Environment env){
       this.env = env;
   }

   public void sendMail(String mailAddress, String title, String mailMessage){
       System.out.print("send");
       SimpleMailMessage message = new SimpleMailMessage();
       message.setFrom(env.getProperty("spring.mail.name"));
       message.setTo(mailAddress);
       message.setSubject(title);
       message.setText(mailMessage);


       System.out.print("sending");
       javaMailSender.send(message);
       System.out.print("sent");
   }
}
