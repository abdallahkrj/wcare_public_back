package com.example.wcare.util;

import com.example.wcare.dto.procedures.ProcedureMetaDataDTO;
import com.example.wcare.data.entities.Procedure;
import com.example.wcare.data.entities.SpecialityTypes;
import com.example.wcare.data.entities.User;
import com.example.wcare.data.entities.UserProcedureTypes;
import com.example.wcare.data.object.SubTypeObject;
import com.example.wcare.data.object.TypeObject;
import com.example.wcare.repositories.SpecialityTypesRepo;
import com.example.wcare.repositories.UserProcedureTypesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class CustomMapper {
    private CustomMapper() {}

    static CustomMapper INSTANCE = new CustomMapper();

    @Autowired
    private UserProcedureTypesRepo userProcedureTypesRepo;
    @Autowired
    private SpecialityTypesRepo specialityTypesRepo;

    public ProcedureMetaDataDTO entityToDTO(Procedure procedure) {
        User user = procedure.getUser();
        UserProcedureTypes procedureTypes = userProcedureTypesRepo.findByUserAndIsDeletedFalse(user).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User Procedure Type not found"));
        SpecialityTypes specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(user.getSpeciality()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Speciality Types not found"));
        String mainType = specialityTypes.getMainTypes().stream().filter(typeObject -> typeObject.id().equals(procedure.getMainTypeId())).findAny().orElseThrow().type();
        String subType = null;
        if (procedure.getSubTypeId() != null) {
            for (SubTypeObject subTypeList : procedureTypes.getSubTypes()) {
                if (subTypeList.mainId().equals(procedure.getMainTypeId())) {
                    for (TypeObject sub : subTypeList.subTypes()) {
                        if (sub.id().equals(procedure.getSubTypeId())) {
                            subType = sub.type();
                        }
                    }
                }
            }
        }
        return ProcedureMetaDataDTO.builder()
                .id(procedure.getId())
                .details(procedure.getDetails())
                .procedureDate(procedure.getProcedureDate())
                .mainType(mainType)
                .subType(subType)
                .userId(procedure.getUser().getId())
                .clinicId(procedure.getClinic().getId())
                .patientId(procedure.getPatient().getId())
                .build();
    }
}
