package com.example.wcare.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableGlobalMethodSecurity(jsr250Enabled = true)
@EnableWebSecurity
public class SecurityFilterChainConfig {
    @Autowired
    JwtAuthenticationFilter jwtAuthFilter;
    @Autowired
    private AuthenticationProvider authenticationProvider;
    private static final String[] swaggerWhiteList = {
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/v2/api-docs/**",
            "/swagger-resources/**",
    };

//    @Bean
//    CorsConfigurationSource corsConfigurationSource() {
//        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(List.of("http://localhost:65201"));
//        configuration.setAllowedMethods(List.of("GET","POST", "OPTIONS"));
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", configuration);
//        return source;
//    }

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable)
                .sessionManagement(sessionManagementConfigurer -> sessionManagementConfigurer
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(authorize -> authorize
//                        .requestMatchers("api/user/create").permitAll() //TODO: role check
                        .requestMatchers(request -> request.getRequestURI().contains("admin")).permitAll()
                        .requestMatchers("api/user/login").permitAll() //TODO: role check
                        .requestMatchers("api/user/forgetPassword/**").permitAll() //TODO: role check
                        .requestMatchers("api/privileges/create").permitAll() //TODO: role check
                        .requestMatchers("api/privileges/getAll").permitAll() //TODO: role check
                        .requestMatchers("api/clinic/create").permitAll() //TODO: role check
                        .requestMatchers("api/plan/**").permitAll() //TODO: role check
                        .requestMatchers("api/file/**").permitAll() //TODO: role check
                        .requestMatchers("api/patient/login").permitAll() //TODO: role check
                        .requestMatchers("api/patient/completeRegistration").permitAll() //TODO: role check
                        .requestMatchers(swaggerWhiteList).permitAll()
                        .requestMatchers(request -> request.getMethod().equals("OPTIONS")).permitAll()
                        .anyRequest().authenticated())
                //TODO: Does we need this? if not we can remove from ExtendedUserDetailsService the implementation of UserDetailsService and remove the AuthenticationProvider Bean
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }

    @Bean
    GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults(""); // Remove the ROLE_ prefix
    }
}