package com.example.wcare.auth;

import com.example.wcare.auth.user_auth.ExtendedUserDetailsService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private ExtendedUserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain
    ) throws ServletException, IOException {
        final String authHeader = request.getHeader("Authorization");
        final String jwt;
        final String userNumber;
        final String id;
        final Type type;
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            // Not Authorized
            filterChain.doFilter(request, response);
            return;
        }
        jwt = authHeader.substring(7);
        userNumber = jwtUtil.extractNumber(jwt);
        id = jwtUtil.extractId(jwt);
//        type = jwtUtil.extractType(jwt);
        List<String> clinicIds = new ArrayList<>();
        if(request.getHeader("clinicId") != null){
            clinicIds = Arrays.stream(request.getHeader("clinicId").split(",")).toList();
        }
        if (userNumber != null && SecurityContextHolder.getContext().getAuthentication() == null) { // getAuthenticated == null to check if the user not authenticated yet so if already authenticated i don't need to authenticate again
            System.out.println("auth userId: " + id);
            UserDetails userDetails = this.userDetailsService.loadUserById(id, clinicIds);
            System.out.println("userDetails: " + userDetails.getAuthorities());
            if (jwtUtil.isTokenValid(jwt, userDetails)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );
                authToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(request)
                );
                SecurityContextHolder.getContext().setAuthentication(authToken);
                request.setAttribute("userNumber", userNumber);
                request.setAttribute("id", id);
            }
        }
        filterChain.doFilter(request, response);
    }
}