package com.example.wcare.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtUtil {

    @Value("${jwt.secret}")
    private String secretKey;

    //expiration date should be based on subscription plan
    public String generateToken(String number, String id, Type userType, Date expirationDate, Map<String,Object> claims) {
        System.out.println("authtokenId: " + id);
        return Jwts.builder()
                .setClaims(claims)
                .setId(id)
                .setSubject(number)
                .claim("type", userType)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expirationDate)
                .signWith(getSignedKey())
                .compact();
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        String number = extractNumber(token);
        return number.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(String token){
        return extractExpiration(token).before(new Date());
    }


    public String extractNumber(String token) {
        return extractClaim(token, Claims::getSubject);
    }


    public String extractId(String token) {
        return extractClaim(token, Claims::getId);
    }
    public String extractType(String token) {
        return extractClaim(token, claims -> claims.get("type").toString());
    }

    public Date extractExpiration(String token) {
        return extractClaim(token,Claims::getExpiration);
    }

    private <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(getSignedKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Key getSignedKey() {
        byte[] secretBytes = Decoders.BASE64.decode(secretKey);
        return Keys.hmacShaKeyFor(secretBytes);
    }
}