package com.example.wcare.auth;

import com.example.wcare.auth.user_auth.ExtendedUserDetailsService;
import com.example.wcare.repositories.ClinicRoleGroupRepo;
import com.example.wcare.repositories.PatientRepo;
import com.example.wcare.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;

@Configuration
public class AuthConfig {
    @Autowired
    UserRepo userRepo;
    @Autowired
    PatientRepo patientRepo;
    @Autowired
    ClinicRoleGroupRepo clinicRoleGroupRepo;
    @Autowired
    ExtendedUserDetailsService extendedUserDetailsService;

    @Bean
    public AuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(extendedUserDetailsService);
        // Password Encoder Init here
        return authProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
}
