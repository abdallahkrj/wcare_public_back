package com.example.wcare.auth.user_auth;

import com.example.wcare.data.entities.Clinic;
import com.example.wcare.data.entities.ClinicRoleGroup;
import com.example.wcare.data.entities.Privilege;
import com.example.wcare.data.entities.User;
import com.example.wcare.repositories.ClinicRepo;
import com.example.wcare.repositories.ClinicRoleGroupRepo;
import lombok.Builder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;


@Builder
public class UserAuth implements UserDetails {
    private List<String> clinicId;
    private User userData;
    private ClinicRoleGroupRepo clinicRoleGroupRepo;
    private ClinicRepo clinicRepo;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<Clinic> clinics = clinicRepo.findAllByIdAndIsDeletedFalse(clinicId);
        List<ClinicRoleGroup> privileges = clinicRoleGroupRepo.findAllByClinicInAndUsersAndIsDeletedFalse(clinics, userData);
        if(privileges.isEmpty()){
            return List.of();
        }
        ClinicRoleGroup firstClinicUserGroupPrivileges = privileges.remove(0);
        privileges.forEach(clinicUserPrivileges -> firstClinicUserGroupPrivileges.getPrivileges().removeIf(privilege -> !clinicUserPrivileges.getPrivileges().contains(privilege)));
        List<String> privilegesName = new java.util.LinkedList<>(firstClinicUserGroupPrivileges.getPrivileges().stream().map(Privilege::getName).toList());
        privilegesName.add("User");
        return privilegesName.stream().map(SimpleGrantedAuthority::new).toList();
    }

    @Override
    public String getPassword() {
        return userData.getPassword();
    }

    @Override
    public String getUsername() {
        return userData.getNumber(); // used for validation -- see isTokenValid method in JwtUtil
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Date getExpirationDate() {
        return new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 365));
    }
}
