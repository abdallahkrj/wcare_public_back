package com.example.wcare.auth.user_auth;

import com.example.wcare.data.entities.Patient;
import com.example.wcare.data.entities.User;
import com.example.wcare.repositories.ClinicRepo;
import com.example.wcare.repositories.ClinicRoleGroupRepo;
import com.example.wcare.repositories.PatientRepo;
import com.example.wcare.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class ExtendedUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    PatientRepo patientRepo;
    @Autowired
    ClinicRoleGroupRepo clinicRoleGroupRepo;
    @Autowired
    ClinicRepo clinicRepo;

    public UserDetails loadUserById(String id, List<String> clinicIds) throws UsernameNotFoundException {
            Optional<User> user = userRepo.findByIdAndIsDeletedFalse(id);
            if (user.isPresent()) {
                User userData = user.get();
                return UserAuth.builder().clinicRoleGroupRepo(clinicRoleGroupRepo).clinicRepo(clinicRepo).userData(userData).clinicId(clinicIds).build();
            } else {
                Patient patient = patientRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
                return PatientAuth.builder().number(patient.getNumber()).password(patient.getPassword()).build();
            }
    }

    public UserDetails loadUserByNumberAndClinicId(String number, List<String> clinicId) {
        Optional<User> user = userRepo.findByNumberAndIsDeletedFalse(number);
        if (user.isPresent()) {
            // do some additional checking
            User userData = user.get();
            return UserAuth.builder().clinicRoleGroupRepo(clinicRoleGroupRepo).clinicRepo(clinicRepo).userData(userData).clinicId(clinicId).build();
        } else {
            Patient patient = patientRepo.findByNumberAndIsDeletedFalse(number).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
            return PatientAuth.builder().number(patient.getNumber()).password(patient.getPassword()).build();
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return null;
    }
}
