package com.example.wcare.auth.user_auth;

import lombok.Builder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Builder
public class PatientAuth implements UserDetails {

    String number;
    String password;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("Patient"));
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return number; // for validation -- see isTokenValid method in JwtUtil
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Date getExpirationDate() {
        return new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 365)); //TODO: extract based on privileges and policy
    }
}
