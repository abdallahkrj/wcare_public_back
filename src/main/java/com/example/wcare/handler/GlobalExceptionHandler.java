package com.example.wcare.handler;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public Map<String, String> handleValidationException(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        Logger.getGlobal().log(Level.WARNING, "handleValidationException Message: " + ex.getMessage() + "/n" + "StackTrace: " + Arrays.toString(ex.getStackTrace()));
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public Map<String, String> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        Map<String, String> errors = new HashMap<>();
        Logger.getGlobal().log(Level.WARNING, "DataIntegrityViolationException Message: " + ex.getMessage());
        errors.put("message", ex.getMessage());
        return errors;
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ProblemDetail> handleResponseStatusException(ResponseStatusException ex) {
        Logger.getGlobal().log(Level.WARNING, "ResponseStatusException:" + ex.getStatusCode() + ", Message: " + ex.getMessage(), ", Reason: " + ex.getReason());
        return ResponseEntity.status(ex.getStatusCode()).body(ex.getBody());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public Map<String, String> handleUnknownError(Exception ex) {
        Map<String, String> errors = new HashMap<>();
        Logger.getGlobal().log(Level.WARNING, "Unknown Error:" + ex.getClass() + ", Message: " + ex.getMessage() + "\n");
        ex.printStackTrace();
        errors.put("message", "Internal Server Error");
        return errors;
    }
}
