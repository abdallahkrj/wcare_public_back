package com.example.wcare.seeder;

import com.example.wcare.data.entities.*;
import com.example.wcare.data.object.*;
import com.example.wcare.repositories.*;
import com.example.wcare.service.ProcedureService;
import lombok.SneakyThrows;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Component
@Profile("dev-server")
public class initDevServerDatabase implements CommandLineRunner {
    @Autowired
    PrivilegesRepo privilegesRepo;
    @Autowired
    SpecialityTypesRepo specialityTypesRepo;
    @Autowired
    PlanRepo planRepo;
    @Autowired
    ClinicRepo clinicRepo;
    @Autowired
    SubscriptionRepo subscriptionRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    ClinicRoleGroupRepo clinicRoleGroupRepo;
    @Autowired
    PatientRepo patientRepo;
    @Autowired
    MedicalHistoryRepo medicalHistoryRepo;
    @Autowired
    AppointmentRepo appointmentRepo;
    @Autowired
    ProcedureRepo procedureRepo;
    @Autowired
    FileRepo fileRepo;
    @Autowired
    ProcedureService procedureService;
    @Autowired
    UserShiftRepo userShiftRepo;
    @Autowired
    ShiftOffRepo offRepo;
    @Value("${clear.DB}")
    private boolean clear;

    @Override
    public void run(String... args) {
        if (clear) {
            System.out.println("Clearing Database");
            privilegesRepo.deleteAll();
            privilegesRepo.deleteAll();
            specialityTypesRepo.deleteAll();
            planRepo.deleteAll();
            clinicRepo.deleteAll();
            subscriptionRepo.deleteAll();
            userRepo.deleteAll();
            userShiftRepo.deleteAll();
            offRepo.deleteAll();
            clinicRoleGroupRepo.deleteAll();
            patientRepo.deleteAll();
            medicalHistoryRepo.deleteAll();
            appointmentRepo.deleteAll();
            procedureRepo.deleteAll();
            fileRepo.deleteAll();
        }

        System.out.println("App Start: initializing local development database");
        System.out.println("init dev-remote database: privileges");
        loadPrivilegesData();
        System.out.println("init dev-remote database: speciality");
        loadSpecialityData();
        System.out.println("init dev-remote database: plan");
        loadPlanData();
        System.out.println("init dev-remote database: clinic");
        loadClinicData();
        System.out.println("init dev-remote database: users");
        loadUserData();
        System.out.println("init dev-remote database: clinic role group");
        loadClinicRoleGroupData();
        System.out.println("init dev-remote database: patients");
        loadPatientsData();
        System.out.println("init dev-remote database: procedures");
        loadProceduresData();
        System.out.println("init dev-remote database: appointment");
        loadAppointmentData();
        System.out.println("init dev-remote database: shifts");
        loadShiftData();
        System.out.println("init dev-remote database: shift-off");
        loadShiftOffData();
    }


    private void loadPrivilegesData() {
        if (privilegesRepo.count() == 0) {
            privilegesRepo.insert(new Privilege(null, Privileges.addPatient.name(), "User can add new patients"));
            privilegesRepo.insert(new Privilege(null, Privileges.editClinicInfo.name(), "User can edit clinic information"));
            privilegesRepo.insert(new Privilege(null, Privileges.editUsersColors.name(), "User can edit clients colors in clinic"));
            privilegesRepo.insert(new Privilege(null, Privileges.editUsersShifts.name(), "User can edit users shifts"));
            privilegesRepo.insert(new Privilege(null, Privileges.haveShift.name(), "User have shift"));
            privilegesRepo.insert(new Privilege(null, Privileges.editUsersDaysOff.name(), "User can edit users days off"));
            privilegesRepo.insert(new Privilege(null, Privileges.editSubProceduresType.name(), "User can add and edit own procedures subtypes"));
            privilegesRepo.insert(new Privilege(null, Privileges.haveAppointments.name(), "User can have appointments (doctor)"));
            privilegesRepo.insert(new Privilege(null, Privileges.addAppointments.name(), "User can make own appointments"));
            privilegesRepo.insert(new Privilege(null, Privileges.deleteAppointment.name(), "User can delete existing own appointments"));
            privilegesRepo.insert(new Privilege(null, Privileges.viewAppointments.name(), "User can view all appointments of the clinic"));
            privilegesRepo.insert(new Privilege(null, Privileges.editPatientsProcedures.name(), "User can add and edit patient's procedures"));
            privilegesRepo.insert(new Privilege(null, Privileges.editPatientsMedicalHistory.name(), "User can edit patient's medical history"));
            privilegesRepo.insert(new Privilege(null, Privileges.editPatientsHistoryFiles.name(), "User can add and edit patient's files"));
            privilegesRepo.insert(new Privilege(null, Privileges.editClinicsUsers.name(), "User can remove and add users to clinic"));
            privilegesRepo.insert(new Privilege(null, Privileges.editClinicsRoleGroups.name(), "User can add and edit role groups in clinic, and can also move users between role groups"));
            privilegesRepo.insert(new Privilege(null, Privileges.addAppointmentsToOther.name(), "User can make appointments for other users"));
            privilegesRepo.insert(new Privilege(null, Privileges.deleteAppointmentsOfOthers.name(), "User can delete existing appointments of others"));
            privilegesRepo.insert(new Privilege(null, Privileges.editSubProceduresTypeToOthers.name(), "User can add and edit procedures subtypes to all users"));
        }
    }

    private void loadSpecialityData() {
        if (specialityTypesRepo.count() == 0) {
            specialityTypesRepo.insert(SpecialityTypes.builder()
                    .speciality(Specialities.DENTAL)
                    .mainTypes(Set.of(
                            new TypeObject(UUID.randomUUID(), "cosmetic_surgery"),
                            new TypeObject(UUID.randomUUID(), "crown"),
                            new TypeObject(UUID.randomUUID(), "implantation"),
                            new TypeObject(UUID.randomUUID(), "filling"),
                            new TypeObject(UUID.randomUUID(), "file"),
                            new TypeObject(UUID.randomUUID(), "toothFile")
                    ))
                    .build());
        }
    }

    private void loadPlanData() {
        if (planRepo.count() == 0) {
            SubscriptionPlan singleDoctorPlan = SubscriptionPlan.builder()
                    .name("Single doctor plan")
                    .duration(365)
                    .maxUserCount(1)
                    .privileges(privilegesRepo.findByNameInAndIsDeletedFalse(List.of(
                            Privileges.addPatient, Privileges.editClinicInfo,
                            Privileges.editUsersColors, Privileges.editUsersShifts,
                            Privileges.haveShift, Privileges.editUsersDaysOff,
                            Privileges.editSubProceduresType, Privileges.haveAppointments,
                            Privileges.addAppointments, Privileges.deleteAppointment,
                            Privileges.viewAppointments, Privileges.editPatientsProcedures,
                            Privileges.editPatientsMedicalHistory, Privileges.editPatientsHistoryFiles
                    )))
                    .build();
            SubscriptionPlan smallClinicPlan = SubscriptionPlan.builder()
                    .name("Small clinic Plan")
                    .duration(365)
                    .maxUserCount(3)
                    .privileges(privilegesRepo.findByNameInAndIsDeletedFalse(List.of(
                            Privileges.addPatient, Privileges.editClinicInfo,
                            Privileges.editUsersColors, Privileges.editUsersShifts,
                            Privileges.haveShift, Privileges.editUsersDaysOff,
                            Privileges.editSubProceduresType, Privileges.haveAppointments,
                            Privileges.addAppointments, Privileges.deleteAppointment,
                            Privileges.viewAppointments, Privileges.editPatientsProcedures,
                            Privileges.editPatientsMedicalHistory, Privileges.editPatientsHistoryFiles,
                            Privileges.editClinicsUsers, Privileges.editClinicsRoleGroups,
                            Privileges.addAppointmentsToOther, Privileges.deleteAppointmentsOfOthers,
                            Privileges.editSubProceduresTypeToOthers
                    )))
                    .build();
            SubscriptionPlan dentalCenterPlan = SubscriptionPlan.builder()
                    .name("Dental Center Plan")
                    .duration(365)
                    .maxUserCount(8)
                    .privileges(privilegesRepo.findByNameInAndIsDeletedFalse(List.of(
                            Privileges.addPatient, Privileges.editClinicInfo,
                            Privileges.editUsersColors, Privileges.editUsersShifts,
                            Privileges.haveShift, Privileges.editUsersDaysOff,
                            Privileges.editSubProceduresType, Privileges.haveAppointments,
                            Privileges.addAppointments, Privileges.deleteAppointment,
                            Privileges.viewAppointments, Privileges.editPatientsProcedures,
                            Privileges.editPatientsMedicalHistory, Privileges.editPatientsHistoryFiles,
                            Privileges.editClinicsUsers, Privileges.editClinicsRoleGroups,
                            Privileges.addAppointmentsToOther, Privileges.deleteAppointmentsOfOthers,
                            Privileges.editSubProceduresTypeToOthers
                    )))
                    .build();

            planRepo.insert(singleDoctorPlan);
            planRepo.insert(smallClinicPlan);
            planRepo.insert(dentalCenterPlan);
        }
    }

    private void loadClinicData() {
        Optional<SubscriptionPlan> sub = planRepo.findByNameAndIsDeletedFalse("Small clinic Plan");
        if (clinicRepo.count() == 0 && sub.isPresent()) {
            Clinic clinic = clinicRepo.insert(Clinic.builder()
                    .name("WCare Clinic")
                    .number("0912345678")
                    .address("Beirut, Hikma street")
//                            .userColors()
//                            .settings()
//                            .files()
                    .build());
            Subscription subscription = subscriptionRepo.insert(Subscription.builder()
                    .plan(sub.get())
                    .clinic(clinic)
                    .date(LocalDate.now())
                    .build());
            System.out.println("subscription added: " + subscription.toString());
        }
    }

    private void loadUserData() {
        Optional<Clinic> clinic = clinicRepo.findByNameAndIsDeletedFalse("WCare Clinic");
        if (userRepo.count() == 0 && clinic.isPresent()) {
            User user1 = userRepo.insert(User.builder()
                    .firstName("Ahmad")
                    .lastName("Hamd")
                    .email("ahmdHmd12@gmail.com")
                    .speciality(Specialities.DENTAL)
                    .number("0923456789")
                    .password("0923456789")
                    .clinics(List.of(clinic.get()))
                    .build());
            User user2 = userRepo.insert(User.builder()
                    .firstName("Yasser")
                    .lastName("Massri")
                    .email("YasserMsr8@gmail.com")
                    .speciality(Specialities.DENTAL)
                    .number("0934567890")
                    .password("0934567890")
                    .clinics(List.of(clinic.get()))
                    .build());
            userRepo.insert(User.builder()
                    .firstName("Hadi")
                    .lastName("Mohssen")
                    .email("HadiMoh76@gmail.com")
                    .speciality(Specialities.DENTAL)
                    .number("0945678901")
                    .password("0945678901")
                    .clinics(List.of(clinic.get()))
                    .build());
            Clinic clinic1 = clinic.get();
            clinic1.setUserColors(Map.of(user1.getId(), "89F2E4", user2.getId(), "C7A276"));
            clinicRepo.save(clinic1);
        }
    }

    private void loadClinicRoleGroupData() {
        Optional<Clinic> clinic = clinicRepo.findByNameAndIsDeletedFalse("WCare Clinic");
        List<User> users = userRepo.findByNumberInAndIsDeletedFalse(List.of("0923456789", "0934567890", "0945678901"));
        if (clinicRoleGroupRepo.count() == 0 && (long) users.size() == 3 && clinic.isPresent()) {
            clinicRoleGroupRepo.insert(ClinicRoleGroup.builder()
                    .name("Admin")
                    .privileges(clinic.get().plan().getPrivileges())
                    .clinic(clinic.get())
                    .users(List.of(users.get(0)))
                    .build());
            clinicRoleGroupRepo.insert(ClinicRoleGroup.builder()
                    .name("Doctor")
                    .privileges(privilegesRepo.findByNameInAndIsDeletedFalse(List.of(
                            Privileges.haveShift,
                            Privileges.editSubProceduresType,
                            Privileges.haveAppointments,
                            Privileges.addAppointments,
                            Privileges.deleteAppointment,
                            Privileges.viewAppointments,
                            Privileges.editPatientsProcedures,
                            Privileges.editPatientsMedicalHistory,
                            Privileges.editPatientsHistoryFiles
                    )))
                    .clinic(clinic.get())
                    .users(List.of(users.get(1)))
                    .build());
            clinicRoleGroupRepo.insert(ClinicRoleGroup.builder()
                    .name("Secretary")
                    .privileges(privilegesRepo.findByNameInAndIsDeletedFalse(List.of(
                            Privileges.addPatient,
                            Privileges.editUsersColors, Privileges.editUsersShifts,
                            Privileges.editUsersDaysOff,
                            Privileges.viewAppointments,
                            Privileges.addAppointmentsToOther,
                            Privileges.deleteAppointmentsOfOthers,
                            Privileges.editSubProceduresTypeToOthers
                    )))
                    .clinic(clinic.get())
                    .users(List.of(users.get(2)))
                    .build());
        }
    }

    private void loadPatientsData() {
        if (patientRepo.count() == 0) {
            patientRepo.insert(Patient.builder()
                    .firstName("Hamza")
                    .lastName("Omari")
                    .birthDate(LocalDate.of(2003, 1, 1))
                    .address("29 December street")
                    .number("0911223344")
                    .email("HmzaOmrai23@gmail.com")
                    .password("123456")
                    .isActivated(true)
                    .medicalHistories(medicalHistoryRepo.insert(List.of(
                            MedicalHistory.builder()
                                    .type(MedicalHistoryType.ILLNESSES_HISTORY)
                                    .description("Diabetes")
                                    .status(true)
                                    .build(),
                            MedicalHistory.builder()
                                    .type(MedicalHistoryType.ILLNESSES_HISTORY)
                                    .description("High Blood Pressure")
                                    .status(false)
                                    .build(),
                            MedicalHistory.builder()
                                    .type(MedicalHistoryType.GENETIC_HISTORY)
                                    .description("High Blood Pressure in family")
                                    .status(true)
                                    .build()
                    )))
                    .build());
        }
    }

    @SneakyThrows
    private void loadProceduresData() {
        Optional<Clinic> clinic = clinicRepo.findByNameAndIsDeletedFalse("WCare Clinic");
        Optional<Patient> patient = patientRepo.findByNumberAndIsDeletedFalse("0911223344");
        Optional<User> user1 = userRepo.findByNumberAndIsDeletedFalse("0923456789");
        Optional<User> user2 = userRepo.findByNumberAndIsDeletedFalse("0934567890");
        Optional<SpecialityTypes> specialityTypes = specialityTypesRepo.findBySpecialityAndIsDeletedFalse(Specialities.DENTAL);
        if (procedureRepo.count() == 0 && clinic.isPresent() && patient.isPresent() && user1.isPresent() && user2.isPresent() && specialityTypes.isPresent()) {
            procedureRepo.insert(Procedure.builder()
                    .mainTypeId(specialityTypes.get().getTypeUUID("filling"))
                    .user(user1.get())
                    .speciality(Specialities.DENTAL)
                    .clinic(clinic.get())
                    .patient(patient.get())
                    .procedureDate(LocalDateTime.now())
                    .details(DentalProcedure.builder()
                            .toothNumber((short) 12)
                            .description("temporary filling")
                            .build())
                    .build());
            procedureRepo.insert(Procedure.builder()
                    .mainTypeId(specialityTypes.get().getTypeUUID("implantation"))
                    .user(user1.get())
                    .speciality(Specialities.DENTAL)
                    .clinic(clinic.get())
                    .patient(patient.get())
                    .procedureDate(LocalDateTime.now())
                    .details(DentalProcedure.builder()
                            .toothNumber((short) 13)
                            .build())
                    .build());

            try{
                Path file = Paths.get("src/main/resources/seeder/files/teeth_file.jpeg");
                byte[] bytes = Files.readAllBytes(file);
                procedureService.addFile(patient.get().getId(), user1.get().getId(), bytes, clinic.get().getId());

                Path toothFile = Paths.get("src/main/resources/seeder/files/tooth_file.jpeg");
                byte[] toothBytes = Files.readAllBytes(toothFile);
                procedureService.addToothFile((short) 23, patient.get().getId(), user1.get().getId(), toothBytes, clinic.get().getId());
            }catch (IOException e){
                System.out.println("IOException");
            }

        }
    }

    private void loadAppointmentData() {
        Optional<Clinic> clinic = clinicRepo.findByNameAndIsDeletedFalse("WCare Clinic");
        Optional<Patient> patient = patientRepo.findByNumberAndIsDeletedFalse("0911223344");
        Optional<User> user = userRepo.findByNumberAndIsDeletedFalse("0934567890");
        if (appointmentRepo.count() == 0 && clinic.isPresent() && patient.isPresent() && user.isPresent()) {
            appointmentRepo.insert(Appointment.builder()
                    .title("Regular Checkup")
                    .notes(List.of())
                    .dateOfReservation(LocalDateTime.now())
                    .duration(25.0)
                    .summary(Summary.builder()
                            .procedures(procedureRepo.findAllByPatientAndIsDeletedFalse(new ObjectId(patient.get().getId())))
                            .description("implanted 13 and filled 12")
                            .build())
                    .type("checkup")
                    .status(AppointmentStatus.DONE)
                    .clinic(clinic.get())
                    .patient(patient.get())
                    .user(user.get())
                    .build());
        }
    }

    private void loadShiftData() {
        Optional<Clinic> clinic = clinicRepo.findByNameAndIsDeletedFalse("WCare Clinic");
        Optional<User> user1 = userRepo.findByNumberAndIsDeletedFalse("0923456789");
        Optional<User> user = userRepo.findByNumberAndIsDeletedFalse("0934567890");
        if (user.isPresent() && user1.isPresent() && clinic.isPresent()) {
            userShiftRepo.insert(UserShift.builder()
                    .user(user.get())
                    .clinic(clinic.get())
                    .daysShifts(Map.of(
                            DayOfWeek.SATURDAY, DayShift.builder()
                                    .startTime(LocalTime.of(10, 0))
                                    .endTime(LocalTime.of(16, 0)).build(),
                            DayOfWeek.SUNDAY, DayShift.builder()
                                    .startTime(LocalTime.of(10, 0))
                                    .endTime(LocalTime.of(18, 0)).build(),
                            DayOfWeek.THURSDAY, DayShift.builder()
                                    .startTime(LocalTime.of(10, 0))
                                    .endTime(LocalTime.of(18, 0)).build(),
                            DayOfWeek.WEDNESDAY, DayShift.builder()
                                    .startTime(LocalTime.of(12, 0))
                                    .endTime(LocalTime.of(18, 0)).build(),
                            DayOfWeek.TUESDAY, DayShift.builder()
                                    .startTime(LocalTime.of(10, 0))
                                    .endTime(LocalTime.of(15, 0)).build()
                    ))
                    .build());
            userShiftRepo.insert(UserShift.builder()
                    .user(user1.get())
                    .clinic(clinic.get())
                    .daysShifts(Map.of(
                            DayOfWeek.SATURDAY, DayShift.builder()
                                    .startTime(LocalTime.of(16, 0))
                                    .endTime(LocalTime.of(20, 0)).build(),
                            DayOfWeek.MONDAY, DayShift.builder()
                                    .startTime(LocalTime.of(14, 0))
                                    .endTime(LocalTime.of(20, 0)).build(),
                            DayOfWeek.THURSDAY, DayShift.builder()
                                    .startTime(LocalTime.of(14, 0))
                                    .endTime(LocalTime.of(20, 0)).build(),
                            DayOfWeek.WEDNESDAY, DayShift.builder()
                                    .startTime(LocalTime.of(14, 0))
                                    .endTime(LocalTime.of(20, 0)).build()
                    ))
                    .build());
        }
    }

    private void loadShiftOffData() {
        Optional<Clinic> clinic = clinicRepo.findByNameAndIsDeletedFalse("WCare Clinic");
        Optional<User> user = userRepo.findByNumberAndIsDeletedFalse("0934567890");
        if (user.isPresent() && clinic.isPresent()) {
            offRepo.insert(ShiftOff.builder()
                    .clinic(clinic.get())
                    .user(user.get())
                    .from(LocalDateTime.of(2024, 7, 30, 0,0))
                    .to(LocalDateTime.of(2024, 7, 31, 23,59))
                    .build());
        }
    }
}
