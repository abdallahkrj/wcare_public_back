package com.example.wcare.controller;

import com.example.wcare.dto.procedures.ProcedureMetaDataDTO;
import com.example.wcare.dto.procedures.ProcedureMainTypeCreateDTO;
import com.example.wcare.dto.procedures.ProcedureMainTypeDTO;
import com.example.wcare.dto.procedures.ProcedureSubTypeCreateDTO;
import com.example.wcare.data.object.Specialities;
import com.example.wcare.service.ProcedureTypeService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping("api/procedureType")
public class ProcedureTypeController {
    @Autowired
    private ProcedureTypeService procedureTypeService;

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ProcedureMetaDataDTO.class))
            }
    )
    @PostMapping(value = "/add", headers = {"clinicId"})
    public ResponseEntity<?> addSubType(@RequestBody ProcedureSubTypeCreateDTO procedureSubTypeCreateDTO) {
        try {
            return ResponseEntity.ok(procedureTypeService.addSubType(procedureSubTypeCreateDTO));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }


    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProcedureMetaDataDTO.class)))
            }
    )
    @GetMapping(value = "/getAllByUser", headers = {"clinicId"})
    public ResponseEntity<?> getAllByUser(@RequestParam String userId) {
        try {
            return ResponseEntity.ok(procedureTypeService.getAllByUser(userId));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }

    }

    @GetMapping(value = "/dental/getAllByUser", headers = {"clinicId"})
    public ResponseEntity<?> getAllDentalByUser(@RequestParam String userId) {
        try {
            return ResponseEntity.ok(procedureTypeService.getAllDentalByUser(userId));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }

    }

//    @ApiResponse(
//            content = {
//                    @Content(
//                            mediaType = "application/json",
//                            array = @ArraySchema(schema = @Schema(implementation = ProcedureMainTypeDTO.class)))
//            }
//    )
//    @GetMapping(value = "/dental/getAllMain", headers = {"clinicId"})
//    public ResponseEntity<?> getAllMainDental() {
//        try {
//            return ResponseEntity.ok(procedureTypeService.getAllMainBySpecTypeIn(List.of(Specialities.DENTAL)));
//        } catch (ResponseStatusException exception) {
//            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
//        }
//    }


    //ADMIN
    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ProcedureMetaDataDTO.class))
            }
    )
    @PostMapping("/admin/addMainType")
    public ResponseEntity<?> addMainType(@RequestBody ProcedureMainTypeCreateDTO procedureMainTypeCreateDTO) {
        try {
            return ResponseEntity.ok(procedureTypeService.adminCreateMainType(procedureMainTypeCreateDTO));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @PostMapping("/admin/addSpecialityTypes")
    public ResponseEntity<?> addSpecialityTypes(@RequestBody Specialities speciality) {
        try {
            return ResponseEntity.ok(procedureTypeService.addSpecialityTypes(speciality));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProcedureMainTypeDTO.class)))
            }
    )
    @GetMapping("/admin/getAll")
    public ResponseEntity<?> getAllMainAdmin() {
        try {
            return ResponseEntity.ok(procedureTypeService.getAll());
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }
}