package com.example.wcare.controller;

import com.example.wcare.dto.subscriptionPlan.SubscriptionPlanCreateDTO;
import com.example.wcare.dto.subscriptionPlan.SubscriptionPlanDTO;
import com.example.wcare.service.PlanService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping("api/plan")
public class PlanController {
    @Autowired
    private PlanService planService;

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = SubscriptionPlanDTO.class))
            }
    )
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody SubscriptionPlanCreateDTO planDTO) {
        try {
            return ResponseEntity.ok(planService.createPlan(planDTO));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = SubscriptionPlanDTO.class))
            }
    )
    @GetMapping("/getAll")
    public ResponseEntity<?> getAll() {
        try {
            return ResponseEntity.ok(planService.getAllPlans());
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
}
