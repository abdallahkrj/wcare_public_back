package com.example.wcare.controller;

import com.example.wcare.dto.medicalHistory.MedicalHistoryCreateDTO;
import com.example.wcare.dto.medicalHistory.MedicalHistoryDTO;
import com.example.wcare.dto.patient.PatientLoginDTO;
import com.example.wcare.dto.patient.PatientCompleteRegistrationDTO;
import com.example.wcare.dto.patient.PatientDTO;
import com.example.wcare.dto.patient.PatientSignupDTO;
import com.example.wcare.service.PatientService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
@RequestMapping(value = "api/patient")
public class PatientController {
    @Autowired
    private PatientService patientService;


    @PostMapping(value = "/create", headers = {"clinicId"})
    public ResponseEntity<?> createPatient(@RequestBody PatientSignupDTO patient) {
        try {
            return ResponseEntity.ok(patientService.createPatient(patient));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping("/completeRegistration")
    public ResponseEntity<?> completeRegistration(@RequestBody PatientCompleteRegistrationDTO patientCompleteRegistrationDTO) {
        try {
            return ResponseEntity.ok(patientService.completeRegistration(patientCompleteRegistrationDTO));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @PostMapping("/login")
    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = PatientDTO.class))
            }
    )
    public ResponseEntity<?> login(@RequestBody PatientLoginDTO patientLoginDTO) {
        try {
            return ResponseEntity.ok(patientService.login(patientLoginDTO));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @GetMapping(value = "/getAll", headers = "clinicId")
    public ResponseEntity<?> getAll() {
        try {
            return ResponseEntity.ok(patientService.getAll());
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }


    @GetMapping(value = "/getAllByUser", headers = "clinicId")
    public ResponseEntity<?> getAll(@RequestParam ObjectId userId) {
        try {
            return ResponseEntity.ok(patientService.getAllByUser(userId));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @GetMapping("/admin/getAll")
    public ResponseEntity<?> adminGetAll() {
        try {
            return ResponseEntity.ok(patientService.getAll());
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @GetMapping("/admin/getById")
    public ResponseEntity<?> getById(@RequestParam String id) {
        try {
            return ResponseEntity.ok(patientService.getById(id));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = MedicalHistoryDTO.class)))
            }
    )
    @GetMapping(value = "/getMedicalHistory", headers = {"clinicId"})
    public ResponseEntity<?> getMedicalHistory(@RequestParam String id) {
        try {
            return ResponseEntity.ok(patientService.getMedicalHistory(id));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = MedicalHistoryDTO.class)))
            }
    )
    @PostMapping("/addMedicalHistory")
    public ResponseEntity<?> addMedicalHistory(@RequestParam String id, @RequestBody MedicalHistoryCreateDTO medicalHistoryDTO) {
        try {
            return ResponseEntity.ok(patientService.addMedicalHistory(id, medicalHistoryDTO));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @PostMapping(value = "/deleteMedicalHistory", headers = {"clinicId"})
    public ResponseEntity<?> deleteMedicalHistory(@RequestParam String id) {
        try {
            return ResponseEntity.ok(patientService.deleteMedicalHistory(id));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping(value = "/changeMedicalHistoryStatus", headers = {"clinicId"})
    public ResponseEntity<?> changeMedicalHistoryStatus(@RequestParam String id, @RequestParam boolean status) {
        try {
            return ResponseEntity.ok(patientService.changeMedicalHistoryStatus(id, status));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
}
