package com.example.wcare.controller;

import com.example.wcare.dto.privileges.PrivilegesCreateDTO;
import com.example.wcare.service.PrivilegesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping("api/privileges")
public class PrivilegesController {


    @Autowired
    private PrivilegesService privilegesService;

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody PrivilegesCreateDTO privilegesDTO) {
        try {
            return ResponseEntity.ok(privilegesService.create(privilegesDTO));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @GetMapping("/getAll")
    public ResponseEntity<?> getAll() {
        try {
            return ResponseEntity.ok(privilegesService.getAll());
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @GetMapping("/getByPlan")
    public ResponseEntity<?> getByPlan(@RequestParam String planId) {
        try {
            return ResponseEntity.ok(privilegesService.getByPlan(planId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

}
