package com.example.wcare.controller;

import com.example.wcare.dto.procedures.DentalProceduresCreateDTO;
import com.example.wcare.dto.procedures.ProcedureDataDTO;
import com.example.wcare.dto.procedures.ProcedureMetaDataDTO;
import com.example.wcare.dto.procedures.ProceduresByToothDTO;
import com.example.wcare.data.object.Specialities;
import com.example.wcare.service.ProcedureService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@CrossOrigin
@RequestMapping("api/procedure")
public class ProcedureController {
    @Autowired
    private ProcedureService procedureService;

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ProcedureMetaDataDTO.class))
            }
    )
    @PostMapping("/dental/create")
    public ResponseEntity<?> createProcedure(@RequestBody DentalProceduresCreateDTO dentalProceduresCreateDTO, @RequestHeader String clinicId) {
        try {
            return ResponseEntity.ok(procedureService.createDentalProcedure(dentalProceduresCreateDTO, clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }


    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProcedureMetaDataDTO.class)))
            }
    )
    @GetMapping("/dental/getAllByPatientAndClinic")
    public ResponseEntity<?> dentalGetAllByPatientAndClinic(@RequestParam ObjectId patientId, @RequestHeader ObjectId clinicId) {
        try {
            return ResponseEntity.ok(procedureService.getAllByPatientAndClinicAndType(patientId, clinicId, Specialities.DENTAL));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProceduresByToothDTO.class)))
            }
    )
    @GetMapping("/dental/getByTooth")
    public ResponseEntity<?> getByTooth(@RequestParam ObjectId patientId, @RequestHeader ObjectId clinicId) {
        try {
            return ResponseEntity.ok(procedureService.getAllByPatientAndClinicAndTypeByTooth(patientId, clinicId));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }

    @GetMapping("/dental/getFiles")
    public ResponseEntity<?> getFiles(@RequestParam ObjectId patientId, @RequestHeader ObjectId clinicId) {
        try {
            return ResponseEntity.ok(procedureService.getAllFilesByPatientAndClinicAndType(patientId, clinicId));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }
    @GetMapping("/dental/getToothFiles")
    public ResponseEntity<?> getToothFiles(@RequestParam ObjectId patientId, @RequestHeader ObjectId clinicId) {
        try {
            return ResponseEntity.ok(procedureService.getAllToothFileByPatientAndClinicAndTypeByTooth(patientId, clinicId));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }


    @PostMapping("/dental/addFile")
    public ResponseEntity<?> addFile(@RequestParam String patientId, @RequestParam String userId, @RequestParam MultipartFile file, @RequestHeader String clinicId) throws IOException {
            return ResponseEntity.ok(procedureService.addFile(patientId, userId, file.getBytes(), clinicId));

    }
    @PostMapping("/dental/addToothFile")
    public ResponseEntity<?> addToothFile(@RequestParam short toothNumber, @RequestParam String patientId, @RequestParam String userId, @RequestParam MultipartFile file, @RequestHeader String clinicId) throws IOException {
        return ResponseEntity.ok(procedureService.addToothFile(toothNumber, patientId, userId, file.getBytes(), clinicId));
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProcedureDataDTO.class)))
            }
    )
    @GetMapping("/patient/getAllByPatient")
    public ResponseEntity<?> dentalGetAllByPatient(@RequestAttribute("id") ObjectId id) {
        try {
            return ResponseEntity.ok(procedureService.getAllByPatientAndType(id, Specialities.DENTAL));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }
//    @GetMapping("/patient/getFiles")
//    public ResponseEntity<?> getPatientFiles(@RequestAttribute ObjectId id, @RequestHeader String clinicId) {
//        try {
//            return ResponseEntity.ok(procedureService.getAllFilesByPatient(id));
//        } catch (ResponseStatusException exception) {
//            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
//        }
//    }
//    @GetMapping("/patient/getToothFiles")
//    public ResponseEntity<?> getPatientToothFiles(@RequestAttribute ObjectId id, @RequestHeader String clinicId) {
//        try {
//            return ResponseEntity.ok(procedureService.getAllToothFileByPatientByTooth(id));
//        } catch (ResponseStatusException exception) {
//            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
//        }
//    }
}