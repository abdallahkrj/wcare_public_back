package com.example.wcare.controller;

import com.example.wcare.dto.clinic.*;
import com.example.wcare.service.ClinicService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("api/clinic")
public class ClinicController {


    @Autowired
    private ClinicService clinicService;

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ClinicDTO.class)))
            }
    )
    @GetMapping("/getAll")
    public ResponseEntity<List<ClinicDTO>> getClinics() {
        return ResponseEntity.ok(clinicService.getAll());

    }
    @PostMapping("/create")
    public ResponseEntity<ClinicDTO> create(@RequestBody ClinicDTO clinic) {
        return ResponseEntity.ok(clinicService.create(clinic));

    }

    @PostMapping("/update")
    public ResponseEntity<ClinicDTO> update(@RequestBody ClinicUpdateDTO clinic, @RequestHeader String clinicId) {
        return ResponseEntity.ok(clinicService.update(clinic, clinicId));
    }



    @GetMapping("/users")
    public ResponseEntity<List<ClinicUserDTO>> getUsers(@RequestHeader String clinicId) {
        return ResponseEntity.ok(clinicService.getUsers(clinicId));
    }

    @GetMapping("/shifts")
    public ResponseEntity<ShiftsDTO> getShifts(@RequestHeader String clinicId) {
        return ResponseEntity.ok(clinicService.getShifts(clinicId));
    }

    @PutMapping("/shifts")
    public ResponseEntity<UserShiftDTO> putShift(@RequestBody UserShiftDTO userShift) {
        return ResponseEntity.ok(clinicService.putShift(userShift));
    }
    @DeleteMapping("/shifts")
    public ResponseEntity<Map<String, String>> deleteShift(@RequestParam String id) {
        return ResponseEntity.ok(clinicService.deleteShift(id));
    }

    @PostMapping("/shiftOff")
    public ResponseEntity<ShiftOffDTO> addShiftOff(@RequestBody ShiftOffDTO shiftOff) {
        return ResponseEntity.ok(clinicService.addShiftOff(shiftOff));
    }
    @DeleteMapping("/shiftOff")
    public ResponseEntity<Map<String, String>> deleteShiftOff(@RequestParam String id) {
        return ResponseEntity.ok(clinicService.deleteShiftOff(id));
    }
}
