package com.example.wcare.controller;

import com.example.wcare.data.object.Privileges;
import com.example.wcare.dto.appointment.AppointmentCreateDTO;
import com.example.wcare.dto.appointment.AppointmentDTO;
import com.example.wcare.dto.appointment.AppointmentUpdateDTO;
import com.example.wcare.data.object.AppointmentStatus;
import com.example.wcare.service.AppointmentService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.annotation.security.RolesAllowed;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/appointment")
public class AppointmentController {
    @Autowired
    private AppointmentService appointmentService;

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AppointmentDTO.class)))
            }
    )
    @PostMapping(value = "/create", headers = "clinicId")
    @RolesAllowed("User")
    public ResponseEntity<?> createAppointment(@RequestBody AppointmentCreateDTO appointment) {
        return ResponseEntity.ok(appointmentService.createAppointment(appointment));
    }

    @ApiResponse()
    @PostMapping(value = "/patient/create")
    @RolesAllowed("Patient")
    public ResponseEntity<?> patientCreate(@RequestBody AppointmentCreateDTO appointment) {
        return ResponseEntity.ok(appointmentService.patientCreateAppointment(appointment));
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = AppointmentDTO.class))
            }
    )
    @PutMapping(value = "/update", headers = "clinicId")
    @RolesAllowed("User")
    public ResponseEntity<?> updateAppointment(@RequestBody AppointmentUpdateDTO appointment, @RequestParam String id) {
        try {
            return ResponseEntity.ok(appointmentService.updateAppointment(appointment, id));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AppointmentDTO.class)))
            }
    )
    @GetMapping(value = "/getByUser", headers = "clinicId")
    @RolesAllowed("User")
    public ResponseEntity<?> getAppointmentsByUser(@RequestParam String userId, @RequestParam(required = false) List<AppointmentStatus> status) {
        try {
            System.out.print(status);
            return ResponseEntity.ok(appointmentService.getAppointmentsByUserAndStatusIn(userId, status));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }

//    @ApiResponse(
//            content = {
//                    @Content(
//                            mediaType = "application/json",
//                            array = @ArraySchema(schema = @Schema(implementation = AppointmentDTO.class)))
//            }
//    )
//    @GetMapping("/getByUserInAndClinicIn")
//    public ResponseEntity<?> getAllByUserInAndClinicInAndStatusIn(@RequestParam List<String> userId, @RequestParam List<String> clinic, @RequestParam(required = false) List<AppointmentStatus> status) {
//        try {
//            return ResponseEntity.ok(appointmentService.getAllByUserInAndClinicInAndStatusIn(userId, clinic, status));
//        } catch (ResponseStatusException exception) {
//            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
//        }
@ApiResponse(
        content = {
                @Content(
                        mediaType = "application/json",
                        array = @ArraySchema(schema = @Schema(implementation = AppointmentDTO.class)))
        }
)
@GetMapping(value = "/getByClinic")
@RolesAllowed({"viewAppointments", "User"})
public ResponseEntity<?> getAllByClinicAndStatusIn(@RequestHeader String clinicId, @RequestParam(required = false) List<AppointmentStatus> status) {
    return ResponseEntity.ok(appointmentService.getAllByClinicAndStatusIn(clinicId, status));
}

//    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AppointmentDTO.class)))
            }
    )
    @GetMapping(value = "/getByUserAndDate", headers = "clinicId")
    public ResponseEntity<?> getByUserAndByDate(@RequestParam String userId, @RequestParam(required = false) List<AppointmentStatus> status, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        try {
            return ResponseEntity.ok(appointmentService.getAppointmentsByDate(userId, status, from, to));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AppointmentDTO.class)))
            }
    )
    @GetMapping(value = "/getByPatientAndDate", headers = "clinicId")
    @RolesAllowed("User")
    public ResponseEntity<?> getByPatientAndByDate(@RequestParam ObjectId patientId, @RequestParam(required = false) List<AppointmentStatus> status, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        try {
            return ResponseEntity.ok(appointmentService.getAppointmentsByPatientAndDate(patientId, status, from, to));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = AppointmentDTO.class)))
            }
    )
    @GetMapping(value = "/patient/getAllByPatient")
    @RolesAllowed("Patient")
    public ResponseEntity<List<AppointmentDTO>> getAllByPatient(@RequestAttribute ObjectId id, @RequestParam(required = false) List<AppointmentStatus> status) {
        return ResponseEntity.ok(appointmentService.getAppointmentsByPatient(id, status));
    }


}