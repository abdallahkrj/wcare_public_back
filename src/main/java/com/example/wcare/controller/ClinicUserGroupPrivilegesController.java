package com.example.wcare.controller;

import com.example.wcare.dto.clinicUserGroupPrivileges.ClinicUserGroupPrivilegesCreateDTO;
import com.example.wcare.dto.clinicUserGroupPrivileges.ClinicUserGroupPrivilegesDTO;
import com.example.wcare.service.ClinicRoleGroupService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("api/clinicUserGroupPrivileges")
public class ClinicUserGroupPrivilegesController {
    @Autowired
    private ClinicRoleGroupService clinicRoleGroupService;

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ClinicUserGroupPrivilegesDTO.class))
            }
    )
    @PostMapping(value = "/create", headers = {"clinicId"})
    public ResponseEntity<?> createGroup(@RequestBody ClinicUserGroupPrivilegesCreateDTO createDTO, @RequestAttribute("userNumber") String userNumber, @RequestHeader String clinicId) {
        try {
            System.out.println("controller");
            return ResponseEntity.ok(clinicRoleGroupService.createClinicUserPrivileges(createDTO, userNumber, clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ClinicUserGroupPrivilegesDTO.class))
            }
    )
    @GetMapping("/getAllByClinic")
    public ResponseEntity<?> getAllByClinic(@RequestHeader String clinicId) {
        try {
            return ResponseEntity.ok(clinicRoleGroupService.getAllByClinic(clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ClinicUserGroupPrivilegesDTO.class))
            }
    )
    @GetMapping(value = "/getAllByUser", headers = {"clinicId"})
    public ResponseEntity<?> getAllByUser(@RequestParam String userId) {
        try {
            return ResponseEntity.ok(clinicRoleGroupService.getAllByUser(userId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }



    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ClinicUserGroupPrivilegesDTO.class))
            }
    )
    @PutMapping("/update")
    public ResponseEntity<?> updateGroup(@RequestParam String id, @RequestBody List<String> privilegesIds) {
        try {
            return ResponseEntity.ok(clinicRoleGroupService.updateGroup(id, privilegesIds));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestParam String id) {
        try {
            return ResponseEntity.ok(clinicRoleGroupService.deleteGroup(id));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ClinicUserGroupPrivilegesDTO.class))
            }
    )
    @PostMapping("/admin/create")
    public ResponseEntity<?> adminCreateGroup(@RequestBody ClinicUserGroupPrivilegesCreateDTO createDTO) {
        try {
            return ResponseEntity.ok(clinicRoleGroupService.adminCreateClinicUserPrivileges(createDTO));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ClinicUserGroupPrivilegesDTO.class))
            }
    )
    @GetMapping("/admin/getAllByClinic")
    public ResponseEntity<?> adminGetAll(@RequestParam String clinicId) {
        try {
            return ResponseEntity.ok(clinicRoleGroupService.getAllByClinic(clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
}