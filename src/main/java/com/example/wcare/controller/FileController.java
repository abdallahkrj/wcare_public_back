package com.example.wcare.controller;

import com.example.wcare.dto.procedures.ProcedureMainTypeDTO;
import com.example.wcare.service.FileService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@RestController
@RequestMapping("api/file")
public class FileController {
    @Autowired
    private FileService fileService;

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = ProcedureMainTypeDTO.class)))
            }
    )
    @GetMapping(value = "/{id}", produces = "application/octet-stream")
    public ResponseEntity<?> getById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(fileService.getById(id));
        } catch (ResponseStatusException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody()); //TODO: make error dto
        }
    }
}