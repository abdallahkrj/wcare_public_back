package com.example.wcare.controller;

import com.example.wcare.dto.code.CodeDTO;
import com.example.wcare.dto.code.PasswordResetDTO;
import com.example.wcare.dto.user.*;
import com.example.wcare.service.UserService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.annotation.security.RolesAllowed;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
@RequestMapping(value = "api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/checkAuth", headers = {"clinicId"})
    @RolesAllowed("viewAppointments")
    public ResponseEntity<?> checkAuth() {
        try {
            return ResponseEntity.ok("ok");
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping(value = "/admin/create")
        public ResponseEntity<?> adminCreate(@RequestBody @Valid UserSignupDTO userSignupDTO, @RequestHeader String clinicId) {
        try {
            return ResponseEntity.ok(userService.adminCreate(userSignupDTO, clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping(value = "/create")
    @RolesAllowed("addUser")
    public ResponseEntity<?> create(@RequestBody @Valid UserSignupDTO userSignupDTO, @RequestAttribute("userNumber") String userNumber, @RequestHeader String clinicId) {
        // get user who make the creation to prevent the user to give privileges that he not has in this clinic
        try {
            System.out.println("userNumber: " + userNumber);
            return ResponseEntity.ok(userService.create(userSignupDTO, userNumber, clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping(value = "/addToClinic", headers = {"clinicId"})
    @RolesAllowed({"addUser"})
    public ResponseEntity<?> addToClinic(@RequestBody UserAddToClinicDTO userAddToClinicDTO, @RequestHeader String clinicId) {
        // get user who make the creation to prevent the user to give privileges that he not has in this clinic
        try {
            return ResponseEntity.ok(userService.addUserToClinic(userAddToClinicDTO, clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserLoginDTO userLoginDTO) {
        return ResponseEntity.ok(userService.login(userLoginDTO));
    }

    @GetMapping("/forgetPassword/demand")
    public ResponseEntity<?> forgetPasswordDemand(@RequestParam String email) {
        try {
            userService.forgetPasswordDemand(email);
            return ResponseEntity.ok("ok");
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping("/forgetPassword/codeVerification")
    public ResponseEntity<?> codeVerification(@RequestParam String email, @RequestBody CodeDTO codeDTO) {
        try {
            userService.checkCode(codeDTO.code(), email);
            return ResponseEntity.ok("ok");
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping("/forgetPassword/updatePassword")
    public ResponseEntity<?> updatePassword(@RequestParam String email, @RequestBody PasswordResetDTO passwordResetDTO) {
        try {
            return ResponseEntity.ok(userService.updatePassword(email, passwordResetDTO.code(), passwordResetDTO.password()));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @GetMapping(value = "/getAll", headers = {"clinicId"})
    public ResponseEntity<?> getAll() {
        try {
            return ResponseEntity.ok(userService.getAll());
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = UserDTO.class)))
            }
    )
    @GetMapping(value = "/getByClinic", headers = {"clinicId"})
    public ResponseEntity<?> getAllByClinic(@RequestHeader("clinicId") String clinicId, @RequestParam(required = false) String privilege) {
        try {
            return ResponseEntity.ok(userService.getAllByClinic(clinicId, privilege));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @ApiResponse(
            content = {
                    @Content(
                            mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = UserDTO.class)))
            }
    )
    @GetMapping(value = "/getByNotInClinic", headers = {"clinicId"})
    public ResponseEntity<?> getByNotInClinic(@RequestHeader("clinicId") String clinicId, @RequestParam(required = false) String privilege) {
        try {
            return ResponseEntity.ok(userService.getAllByNotInClinic(clinicId, privilege));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @GetMapping("/admin/getAll")
    public ResponseEntity<?> adminGetAll() {
        try {
            return ResponseEntity.ok(userService.getAll());
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }


    @PostMapping(value = "/update", headers = {"clinicId"})
    public ResponseEntity<?> update(@RequestBody UserUpdateDTO userUpdateDTO, @RequestParam String userId) {
        try {
            return ResponseEntity.ok(userService.updateUser(userUpdateDTO, userId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
    @PostMapping(value = "/changeGroup", headers = {"clinicId"})
    public ResponseEntity<?> changeGroup(@RequestBody UserChangeGroupDTO userChangeGroupDTO, @RequestHeader("clinicId") String clinicId) {
        try {
            return ResponseEntity.ok(userService.changeGroup(userChangeGroupDTO, clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }

    @PostMapping(value = "/admin/addToClinic", headers = {"clinicId"})
    public ResponseEntity<?> adminAddToClinic(@RequestBody UserAddToClinicDTO userAddToClinicDTO, @RequestHeader String clinicId) {
        // get user who make the creation to prevent the user to give privileges that he not has in this clinic
        try {
            return ResponseEntity.ok(userService.addUserToClinic(userAddToClinicDTO, clinicId));
        } catch (ResponseStatusException exception) {
            exception.printStackTrace();
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getBody());
        }
    }
}
